This program was developped during my master in physics at Université Laval
with Michel Piché and Michel Olivier. It aims to predict the propagation of
ultrashort laser pulses in optical fibers. This program gave us the results
that were published in my Master's thesis and in this paper.

This program is command-line only and works as an object-oriented code. It has
been developped with Python 3 on linux (Ubuntu, Manjaro, Fedora). It may also
work on Windows and Mac OS X, but has not been tested on these platform.
In order to work, this program needs a working Python 3 installation with the
addition of scipy and matplotlib libraries. One also needs cython in order to
compile some algorithms that were too slow to be written in interpreted
python code.

On Fedora:
sudo dnf install python3 python3-matplotlib python3-scipy python3-Cython

It is also useful to work into an interactive python environment such as
ipython:

On Fedora:
sudo dnf install python3-ipython

To us this program, one creates a script where he or she defines the initial
pulses to work with as beam.Pulse objects and the fiber as an
evolution.Propagation or evolution.Amplification object with the fiber
caracteristics.

This program uses a Split-step Fourier algorithm in order to approximate
the calculation of dispersion and rare-earth amplification in the frequency
domain and to calculate the nonlinear effects (Raman, self-phase-modulation)
in the time domain. As a consequence, this program relies heavily upon
Fourier transforms. Moreover, for chirped-pulse amplification, one needs to
have high precision in the frequency domain and in the time domain, which
nececitates a high number of points. For these reasons, I chose to use FFTW
(fastest fourier transform in the west), developped by MIT and published under
the MIT license (free for open source and education projects, but non free
for commercial projects). To use this program, one needs to install FFTW
version 3 and pyfftw, which is a wrapper for FFTW in python.

On Fedora :
dnf install fftw
pip install pyfftw

This program is still in (slow) development. The develop branch aims to reduce
the memory consumption by doing the Fourier transforms inplace instead of
having two arrays (one for the frequency domain and one for the time domain).
It already works, but not all the code has yet been adapted for the new
procedure.

Note : The master branch of the program may be outdated and may not work
with recent versions of Python. One may have to adapt it a little to make it work
The develop branch do work with up-to-date Python, but not all classes and
routines have yet been adapted to the new prcedure

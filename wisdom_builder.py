# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function

import warnings, cProfile
from pylab import *
from scipy.constants import c, h, pi

from pyfiber import tools
warnings.simplefilter('ignore')

tools.import_wisdom_from_file('wisdom/Lenovo_Thinkpad_Yoga_L380_10_to_21_FFTW_MEASURE.wis')

Nmin = 10   # from 2**Nmin
Nmax = 21   # to 2**Nmax

flag = 'FFTW_PATIENT'   # FFTW_ESTIMATE FFTW_MEASURE FFTW_PATIENT FFTW_EXHAUSTIVE

for Npts in 2**np.arange(Nmax,Nmin-1,-1):
	print('{} points'.format(Npts))
	tab1 = np.empty((2,Npts), dtype='complex64')
	tab2 = np.empty((2,Npts), dtype='complex64')
	tab3 = np.empty(Npts, dtype='complex64')
	tab4 = np.empty((2,Npts), dtype='complex64')
	
	fftw1 = tools.FFTW(tab1, tab2, fftw_flags=[flag], axes=(-1,))
	print('fftw1 done')
	fftw2 = tools.FFTW(tab3, fftw_flags=[flag])
	print('fftw2 done')
	fftw4 = tools.FFTW(tab4, fftw_flags=[flag], axes=(-1,))
	print('fftw4 done')


tools.export_wisdom_to_file('wisdom/Lenovo_Thinkpad_Yoga_L380_{}_to_{}_{}.wis'.format(Nmin,Nmax,flag))



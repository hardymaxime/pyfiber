#-*-coding:utf8 *-

from pylab import *
import sys
sys.path.append('/home/maxime/Dropbox/ProgrammeFibre')
from outils import *


l = linspace(1500,1580,1000)

lorentzian = lambda x,p: p[2]/(1.+4*((x-p[0])/p[1])**2.)
s21 = lambda l: lorentzian(l,[1530,10,5]) + lorentzian(l,[1550,30,3])
s12 = lambda l: exp(1e5*(1./l - 1./1530))*s21(l)

plot(l,s21(l))
plot(l,s12(l))

show()

N = 1.97e25
gain = lambda l, Kth, Ksat: N*(s21*Kth - s12*Ksat)/(1. + Kth + Ksat)



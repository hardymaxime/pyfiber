# -*- coding: utf-8 -*-

from pylab import *
from scipy.interpolate import interp1d
import tools

absorption = loadtxt('er30-4.csv',delimiter=',',skiprows=1).T
emission = loadtxt('er30-4_emission.csv',delimiter=',',skiprows=1).T


normalisation = 5.1 / max(absorption[1]) # selon Barnes1991

absorption[1] *= normalisation
emission[1] *= normalisation

plot(absorption[0],absorption[1],'r-',lw=2,label="Absorption")
plot(emission[0],emission[1],'b-',lw=2,label="\\'Emission")

xlabel('$\lambda$ (nm)',fontsize=18)
ylabel('Section efficace $\sigma \; (\\times 10^{-25} \; \metre^2)$',fontsize=18)

legend()

#tools.save_pdf(gcf(),'sections_efficaces_Thorlabs.pdf')


# Courbes de gain pour laser continu

s12 = interp1d(absorption[0],absorption[1])
s21 = interp1d(emission[0],emission[1])

N = 1.97
gain = lambda l, Kth, Ksat: N*(s21(l)*Kth - s12(l))/(1. + Kth + Ksat)

plt.figure()

l = linspace(1450,1615,1000)

for Kth in logspace(-1.,1.,10):
	plot(l,gain(l,Kth,0.),lw=1.5)

plot(l,[0]*len(l),'k--')
xlim(l[0],l[-1])

tools.save_pdf(gcf(),'amplification_g_vs_Kp.pdf')


plt.figure()
for Ksat in logspace(-1,1,10):
	plot(l,gain(l,10.,Ksat),lw=1.5)


xlim(l[0],l[-1])

tools.save_pdf(gcf(),'amplification_g_vs_P.pdf')


show()

# -*- coding: utf-8 -*-

import numpy as np
from scipy.constants import hbar, c, pi

from . import tools


def quarter_wavelength_thickness(index, wavelength, incidence_angle, ambiant_index=1., n=1.):
		angle = np.arcsin(ambiant_index * incidence_angle / index)
		return n * 0.25 * wavelength / (index * np.cos(angle))


def half_wavelength_thickness(index, wavelength, incidence_angle, ambiant_index=1., n=1.):
		angle = np.arcsin(ambiant_index * incidence_angle / index)
		return n * 0.5 * wavelength / (index * np.cos(angle))


class ThinLayer(object):
	def __init__(self, index, thickness, wavelength, incidence_angle=0., ambiant_index=1.):
		'''Must give the refractive index, the layer thickness and the void wavelength'''
		self.index = index
		self.thickness = thickness
		self.wavelength = wavelength
		self.angle = np.arcsin(ambiant_index * np.sin(incidence_angle) / self.index)



	def Abeles_matrix(self, wavelength=None, angle=None, pol='p'):
		if wavelength is None: wavelength = self.wavelength
		if angle is None: angle = self.angle

		phase = 2*pi*self.thickness * self.index * np.cos(angle) / wavelength

		if pol == 'p':
			res = np.array([[np.cos(phase), 1j*np.sin(phase)*np.cos(angle)/self.index],\
					[1j*self.index*np.sin(phase)/np.cos(angle), np.cos(phase)]])
		elif pol == 's':
			res = np.array([[np.cos(phase), 1j*np.sin(phase)/(self.index * np.cos(angle))],\
					[1j*self.index*np.cos(angle)*np.sin(phase), np.cos(phase)]])
		else:
			raise AttributeError("'pol' must be either 'p' or 's'")

		
		if hasattr(wavelength, '__len__'):
			return np.rollaxis(res,2)
		else:
			return res




class QuarterWavelengthLayer(ThinLayer):
	def __init__(self, index, wavelength, n=1, incidence_angle=0., ambiant_index=1.):
		self.index = index
		self.wavelength = wavelength
		self.angle = np.arcsin(ambiant_index * incidence_angle / self.index)
		self.thickness = n * 0.25 * wavelength / (index * np.cos(self.angle))


class HalfWavelengthLayer(ThinLayer):
	def __init__(self, index, wavelength, n=1, incidence_angle=0., ambiant_index=1.):
		self.index = index
		self.wavelength = wavelength
		self.angle = np.arcsin(ambiant_index * incidence_angle / self.index)
		self.thickness = n * 0.5 * wavelength / (index * np.cos(self.angle))





class DielectricMirror(object):
	def __init__(self, wavelength, substrate_index, layers, ambiant_index=1., incidence_angle=0.):
		self.wavelength=wavelength
		self.layers = layers

		self.ambiant_index = ambiant_index
		self.substrate_index = substrate_index

		self.incidence_angle = incidence_angle
		self.substrate_angle = np.arcsin(self.ambiant_index * np.sin(self.incidence_angle) / self.substrate_index)

		self._n1 = ambiant_index
		self._nf = substrate_index

	
	def find_angles(self, incidence_angle=None):
		if incidence_angle is None:
			angle_1 = self.incidence_angle
			angle_f = self.substrate_angle
		else:
			angle_1 = incidence_angle
			angle_f = np.arcsin(self.ambiant_index * np.sin(incidence_angle) / self.substrate_index)

		return angle_1, angle_f



	def Abeles_matrix(self, wavelength=None, angle=None, pol='p'):
		if wavelength is None: wavelength = self.wavelength
		if angle is None: angle = self.incidence_angle

		if hasattr(wavelength, '__len__'):
			res = np.array([np.identity(2, dtype='complex64')]*len(wavelength))
		else:
			res = np.identity(2, dtype='complex64')

		for layer in self.layers:
			layer_angle = np.arcsin(self.ambiant_index * angle / layer.index)
			res = tools.matrix_multiply(res, layer.Abeles_matrix(wavelength,layer_angle,pol))

		return res


	def transmission(self, wavelength=None, angle=None, pol='p'):
		AbMat = self.Abeles_matrix(wavelength, angle, pol)
	
		angle_1, angle_f = self.find_angles(angle)

		if hasattr(wavelength, '__len__'):
			AbMat = np.rollaxis(AbMat,0,3)

		if pol == 'p':
			res = (2 * self._n1 * np.cos(angle_1)) /(AbMat[0,0] * self._n1 * np.cos(angle_f) + AbMat[0,1] * self._n1 * self._nf +\
					AbMat[1,0] * np.cos(angle_1) * np.cos(angle_f) + AbMat[1,1] * self._nf * np.cos(angle_1))
		elif pol == 's':
			res = (2 * self._n1 * np.cos(angle_1)) /(AbMat[0,0] * self._n1 * np.cos(angle_1) +\
					AbMat[0,1] * self._n1 * np.cos(angle_1) * self._nf * np.cos(angle_f) +\
					AbMat[1,0] + AbMat[1,1] * self._nf * np.cos(angle_f))
		else:
			raise AttributeError("'pol' must be either 'p' or 's'")


		return res

	
	def power_transmission(self, wavelength=None, angle=None, pol='p'):
		angle_1, angle_f = self.find_angles(angle)
	
		return np.abs(self.transmission(wavelength, angle, pol))**2 *\
				self._nf * np.cos(angle_f) / (self._n1 * np.cos(angle_1))



	def reflexion(self, wavelength=None, angle=None, pol='p'):
		AbMat = self.Abeles_matrix(wavelength, angle, pol)

		angle_1, angle_f = self.find_angles(angle)

		if hasattr(wavelength, '__len__'):
			AbMat = np.rollaxis(AbMat,0,3)

		if pol == 'p':
			res = (AbMat[0,0] * self._n1 * np.cos(angle_f) + AbMat[0,1] * self._n1 * self._nf -\
					AbMat[1,0] * np.cos(angle_1) * np.cos(angle_f) - AbMat[1,1] * self._nf * np.cos(angle_1)) /\
					(AbMat[0,0] * self._n1 * np.cos(angle_f) + AbMat[0,1] * self._n1 * self._nf +\
					AbMat[1,0] * np.cos(angle_1) * np.cos(angle_f) + AbMat[1,1] * self._nf * np.cos(angle_1))
		elif pol == 's':
			res = (AbMat[0,0] * self._n1 * np.cos(angle_1) + AbMat[0,1] * self._n1 * np.cos(angle_1) * self._nf * np.cos(angle_f) -\
					AbMat[1,0] - AbMat[1,1] * self._nf * np.cos(angle_f)) /\
					(AbMat[0,0] * self._n1 * np.cos(angle_1) + AbMat[0,1] *\
					self._n1 * np.cos(angle_1) * self._nf * np.cos(angle_f) +\
					AbMat[1,0] + AbMat[1,1] * self._nf * np.cos(angle_f))
		else:
			raise AttributeError("'pol' must be either 'p' or 's'")


		return res

	
	def power_reflexion(self, wavelength=None, angle=None, pol='p'):
		return np.abs(self.reflexion(wavelength, angle, pol))**2



class BackFaceDielectricMirror(DielectricMirror):
	def __init__(self, wavelength, substrate_index, layers, ambiant_index=1., incidence_angle=0.):
		self.wavelength=wavelength
		self.layers = layers

		self.ambiant_index = ambiant_index
		self.substrate_index = substrate_index

		self.incidence_angle = incidence_angle
		self.substrate_angle = np.arcsin(self.ambiant_index * np.sin(self.incidence_angle) / self.substrate_index)

		self._n1 = substrate_index
		self._nf = ambiant_index

	
	def find_angles(self, incidence_angle=None):
		if incidence_angle is None:
			angle_1 = self.substrate_angle
			angle_f = self.incidence_angle
		else:
			angle_1 = np.arcsin(self.ambiant_index * np.sin(incidence_angle) / self.substrate_index)
			angle_f = incidence_angle

		return angle_1, angle_f


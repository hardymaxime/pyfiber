# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

# Importation des librairies de python nécessaires à la résolution du problème
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.special import jn_zeros	# fonction sortant les m premiers zeros de la fonction de Bessel J_n
from scipy.special import jv,kv 	# fonction de Bessel de première catégorie J_n et K_n
from scipy.optimize import fmin,fsolve	# 2 algorithmes d'optimisation de fonction
from scipy.interpolate import interp1d

from os.path import isfile

from . import tools



# On définit ici une classe permettant de représent un mode comme étant objet ayant ses méthodes propres
# Par exemple, si on veut travailler avec le mode LP_01, on peut définir LP_01 = mode_LP(0,1)

class ModeLP(object):
	def __init__(self, l, m, V=None):
		self.l, self.m = l, m
	
		# Trouver le V de coupure car proche de la coupure, u = V; aussi J_(l-1) (Vc) = 0
		if self.l == 0 and self.m == 1:
			self.Vc = 0
		elif self.l == 0:
			self.Vc = jn_zeros(self.l-1,self.m-1)[-1]
		else:
			self.Vc = jn_zeros(self.l-1,self.m)[-1] 

		if V is not None:
			self.set_V(V)


	def set_V(self, V, Miyagi=False):
		self.V = V

		if Miyagi:
			# Sort le m^ieme zéro de la fonction de Bessel
			u_inf = jn_zeros(self.l,self.m)[self.m-1]
			self.u = u_inf*(V/(V+1))*(1.-u_inf**2./(6.*(V+1.)**3.)-u_inf**4./(20.*(V+1.)**5.))
		else:
			tab_u_V = np.load(tools.open_local_file('data/LP_{}_{}.npy'.format(self.l,self.m)))
			func_u_V = interp1d(tab_u_V[0],tab_u_V[1]) # pose problème à Pickle
			self.u = func_u_V(V)

		self.w = np.sqrt(V**2. - self.u**2.)
		self.set_confinement()
		self.set_radius()

		


	def set_profile(self, r, phi=0., core_radius=1.):
		self.profile = np.zeros(r.shape)
		lim = np.abs(r) < core_radius
	
		try:
			self.profile[lim] = (jv(self.l,self.u*np.abs(r[lim])/core_radius) * np.cos(self.l*phi))**2
			self.profile[-lim] = (jv(self.l,self.u)/kv(self.l,self.w) * kv(self.l,self.w*np.abs(r[-lim])/core_radius) * \
					np.cos(self.l*phi))**2
		except:
			self.profile[lim] = (jv(self.l,self.u*np.abs(r[lim])/core_radius) * np.cos(self.l*phi[lim]))**2
			self.profile[-lim] = (jv(self.l,self.u)/kv(self.l,self.w) * kv(self.l,self.w*np.abs(r[-lim])/core_radius) * \
					np.cos(self.l*phi[-lim]))**2
	
		self.profile /= np.pi * np.sum(self.profile*np.abs(r)) * (r[1]-r[0])

		return self.profile


	# Fonction phi_l(w)
	def _phi_l(self, w):
		return ((kv(self.l,w))**2)/(kv(self.l-1,w)*kv(self.l+1,w))


	# Méthode calculant le facteur de confinement pour le mode selon son V (V est un vecteur)
	def set_confinement(self, Miyagi=False):
		self.confinement = 1-(self.u/self.V)**2*(1-self._phi_l(self.w))
		return self.confinement

	
	def set_radius(self, core_radius=1.):
		self.radius = core_radius * (0.65 + 1.619*self.V**(-1.5) + 2.879*self.V**(-6.))
		return self.radius

	def set_power_radius(self, core_radius=1.):
		assert(self.l == 0. and self.m == 1.)
		self.power_radius = core_radius * (V*kv(1,self.w)*jv(0,self.u))/(self.u*kv(0,w))
		return self.power_radius



class Multimode(object):
	def set_V(self, V, Miyagi=False):
		pass


	def set_radius(self, radius):
		self.radius = radius

	
	def set_confinement(self, confinement):
		self.confinement = confinement



def solve_u(mode, V, approx=None):
	tab_u = np.zeros(len(V))
	# Si un approché 'approx' n'est pas donné:
	# on essaie d'abord avec u = Vc.
	# Au fur et à mesure que ça avance, on utilise le u trouvé précédemment comme approximation de départ.
	if approx is None:
		approx = mode.Vc()

	for i in range(len(V)):
		f = lambda u: np.abs(u*jv(mode.l-1,u)/kv(mode.l-1,np.sqrt(V[i]**2-u**2))+\
				np.sqrt(V[i]**2-u**2)*jv(mode.l,u)/kv(mode.l,np.sqrt(V[i]**2-u**2)))
		# Comme il y a un problème avec la résolution numérique du LP_01 mode, 
		# on utilise un algorithme différent
		if mode.l == 0 and mode.m == 1:
			# fmin = algorithme faisant intervenir la méthode du simplexe
			u = fmin(f,approx,disp=False)[0]
		else:
			# fsolve = algorithme standard de scipy pour trouver les zéros 
			# d'une équation non linéaire
			u = fsolve(f,approx,xtol=1e-6)[0]	
		
		# Lorsque u n'a pas convergé il est 'nan' (soit 'not a number') 
		# et donc ne peut pas servir à déterminer le point de départ de la prochaine
		# itération
		if not np.isnan(u):
			approx = u
		tab_u[i] = u

	np.save('/home/maxime/Dropbox/ProgrammeFibre/data/LP_{}_{}.npy'.format(mode.l,mode.m),array([V,tab_u]))


# Méthode calculant la constante de propagation normalisée b
def find_b(mode, V, Miyagi=False):
	return (V**2-(mode.u(V,Miyagi))**2)/(V**2)




# Méthode calculant le facteur V(Vb)'' pour le mode
def find_VVbpp(mode, V, Miyagi=False):
	# Fonction phi_l(w)
	phi = lambda w: ((kv(mode._l,w))**2)/(kv(mode._l-1,w)*kv(mode._l+1,w))
	
	u = mode.u(V,Miyagi)
	w = np.sqrt(V**2 - u**2)
	return 2*(u/V)**2*(phi(w)*(1-2*phi(w))+(2/w)*(w**2 + u**2*phi(w))*np.sqrt(phi(w))*(phi(w)+np.sqrt(phi(w))/w -1))


# Méthode calculant le facteur (Vb)' pour le mode
def find_Vbp(mode, V, Miyagi=False):
	# Fonction phi_l(w)
	phi = lambda w: ((kv(mode._l,w))**2)/(kv(mode._l-1,w)*kv(mode._l+1,w))

	u = mode.u(V,Miyagi)
	w = np.sqrt(V**2 - u**2)
	return 1. - (u/V)**2.*(1.-2.*phi(w))


def mode_shape(mode, V, Miyagi=False):
	x = np.linspace(-2,2,100)
	y = np.linspace(-2,2,100)
	x,y = meshgrid(x,y)
	r = np.sqrt(x**2 + y**2)
	phi = np.arctan2(y,x)

	fig = plt.figure()
	ax = fig.add_subplot(111,projection='3d')
	ax.plot_surface(x,y,mode.I(V,r,phi),cmap=cm.jet,cstride=1,rstride=1,shade=False,antialiased=False,linewidth=0)
	return fig






# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import numpy as np
from scipy.constants import hbar, c, pi
from scipy.interpolate import interp1d
from scipy.fftpack import hilbert

from . import beam, tools


########################## COMPONENTS WITH JONES MATRIX #####################################

class JonesMatrixComponent(object):
	def __call__(self, *beams, **kwargs):
		beams = beam.BeamList(beams).copy(**kwargs)
		Jones_matrix = self.Jones_matrix()

		for each_beam in beams:
			each_beam.set_A(np.dot(Jones_matrix, each_beam.A()))

		return beams

	def Jones_matrix(self):
		return np.array([[1,0], [0,1]])



class Polarizer(JonesMatrixComponent):
	def __init__(self, angle=0., T=1., t=0.):
		# The angle must be given in radians
		self.angle = angle
		self.T = np.sqrt(T)
		self.t = np.sqrt(t)

	def Jones_matrix(self):
		# Il s'agit de la matrice de Jones pour un polariseurs faisant un angle quelconque,
		# remise dans la base des polarisations circulaires
		return 0.5*np.array([[self.T+self.t, (self.T-self.t)*np.exp(-2j*self.angle)],\
				[(self.T-self.t)*np.exp(2j*self.angle), self.T+self.t]])




class PhaseRetarder(JonesMatrixComponent):
	def __init__(self, phase_x=0., phase_y=0., angle=0.):
		# The phases and angles must be given in radians
		self.phase_x = phase_x
		self.phase_y = phase_y
		
		self.angle = angle

	def Jones_matrix(self):
		return 0.5*np.array([[np.exp(1j*self.phase_x)+np.exp(1j*self.phase_y),\
				(np.exp(1j*self.phase_x)-np.exp(1j*self.phase_y))*np.exp(-2j*self.angle)],\
				[(np.exp(1j*self.phase_x)-np.exp(1j*self.phase_y))*np.exp(2j*self.angle),\
				np.exp(1j*self.phase_x)+np.exp(1j*self.phase_y)]])


class HalfWaveplate(PhaseRetarder):
	def __init__(self, angle=0.):
		PhaseRetarder.__init__(self,phase_x=0.,phase_y=pi,angle=angle)


class QuarterWaveplate(PhaseRetarder):
	def __init__(self, angle=0.):
		PhaseRetarder.__init__(self,phase_x=0.,phase_y=pi/2.,angle=angle)



class P_APM(JonesMatrixComponent):
	'''Equivalent to Polarizer + Quarter Waveplate + Half waveplate'''
	def __init__(self, hwp_angle, qwp_angle):
		self.hwp_angle = hwp_angle
		self.qwp_angle = qwp_angle


	def Jones_matrix(self):
		return 0.25 * np.array([[(1 + 1j + (1-1j)*np.exp(2j*self.qwp_angle)) * np.exp(-2j*self.hwp_angle),\
				(1 + 1j + (1-1j)*np.exp(2j*self.qwp_angle)) * np.exp(-2j*self.hwp_angle)],\
				[(1 + 1j + (1-1j)*np.exp(-2j*self.qwp_angle)) * np.exp(2j*self.hwp_angle),\
				(1 + 1j + (1-1j)*np.exp(-2j*self.qwp_angle)) * np.exp(2j*self.hwp_angle)]])
				



########################## COMPONENTS FOR LOSS ##############################################


class TransmissionLoss(object):
	def __init__(self, T=1.):
		self.T = T
	
	def __call__(self, *beams, **kwargs):
		beams = beam.BeamList(beams).copy(**kwargs)

		for each_beam in beams:
			T = self.transmission(each_beam)

			each_beam.time_gain(np.sqrt(T))

		return beams


	def transmission(self, the_beam):
		return self.T



class Splice(TransmissionLoss):
	def __init__(self, fiber1, fiber2):
		self.fiber1 = fiber1
		self.fiber2 = fiber2


	def transmission(self, the_beam):
		w1 = self.fiber1.core_radius *\
				(0.65 + 1.619*self.fiber1.V(the_beam)**(-1.5) + 2.879*self.fiber1.V(the_beam)**(-6.))

		w2 = self.fiber2.core_radius *\
				(0.65 + 1.619*self.fiber2.V(the_beam)**(-1.5) + 2.879*self.fiber2.V(the_beam)**(-6.))

		return ((2.*w1*w2)/(w1**2.+w2**2.))**2.



########################## COMPONENTS FOR DELAY #############################################

class DispersiveDelayLine(object):
	def __init__(self, beta):
		self.beta = beta

	def __call__(self, *beams, **kwargs):
		beams = beam.BeamList(beams).copy(**kwargs)

		for each_beam in beams:
			each_beam.freq_gain(np.exp(1j * self.beta(each_beam)))

		return beams


class GratingPair(DispersiveDelayLine):
	def __init__(self, groove_density, distance, incidence_angle, wavelength, order=-1, beta_order=3):
		self.groove_density = groove_density
		self.distance = distance
		self.incidence_angle = incidence_angle
		self.wavelength = wavelength
		self.order = order
		self.beta_order = beta_order

		self.beta = tools.Beta(wavelength, [0.]*(beta_order+1))
		self.compute_beta()


	def compute_beta(self):
		self.diffraction_angle = np.arcsin(np.sin(self.incidence_angle) + self.order * self.wavelength * self.groove_density)
		self.deviation_angle = self.incidence_angle + self.diffraction_angle

		# The factor 2 for beta[2] is because the beam passes through the grating pair twice
		self.beta[2] = - 2 * (self.wavelength / (2*pi*c**2)) * (self.wavelength * self.groove_density)**2 *\
				self.distance / (np.cos(self.diffraction_angle))**3

		for i in range(3, self.beta_order+1):
			self.beta[i] = -3 * (self.wavelength / (2*pi*c * (np.cos(self.diffraction_angle))**2)) * self.beta[i-1] *\
					((np.cos(self.diffraction_angle))**2 + self.wavelength*self.groove_density *\
					(self.wavelength*self.groove_density + np.sin(self.incidence_angle)))


		return self.beta
	

	def find_optimal_distance(self, beta_2):
		self.distance = - 0.5 * beta_2 * (2*pi*c**2/self.wavelength) * (np.cos(self.diffraction_angle))**3 /\
				(self.wavelength * self.groove_density)**2

		self.compute_beta()

		return self.distance
	


		
class DelayLine(object):
	def __init__(self, delay):
		self.delay = delay

	def __call__(self, *beams, **kwargs):
		beams = beam.BeamList(beams).copy(**kwargs)

		for each_beam in beams:
			each_beam.freq_gain(np.exp(1j * each_beam.w * self.delay))

		return beams


########################## FILTERS ##########################################################

class Filter(object):
	def __call__(self, *beams, **kwargs):
		beams = beam.BeamList(beams).copy(**kwargs)

		if hasattr(self, '_filter_x') and hasattr(self, '_filter_y'):
			for each_beam in beams:
				#Atx = self._filter_x(np.fft.fftshift(each_beam.f)) * (each_beam.At[0] + each_beam.At[1]) / np.sqrt(2)
				#Aty = -1j * self._filter_y(np.fft.fftshift(each_beam.f)) * (each_beam.At[0] - each_beam.At[1]) / np.sqrt(2)

				#each_beam.init_At(np.array([Atx,Aty]))

				# Marche avec le nouveau code 20 jan 2016
				At = self._filter_x(each_beam.f) * each_beam.At(pol='x') + self._filter_y(each_beam.f) * each_beam.At(pol='y')
				each_beam.set_At(At)
		else:
			for each_beam in beams:
				each_beam.freq_gain(self._filter(each_beam.f))

		return beams

	def _filter(self, f):
		return np.ones(len(f))




class LowPassFilter(Filter):
	def __init__(self, l0, order=1):
		self.f0 = c/l0
		self.order = order
	
	def _filter_old(self, f): # Non causal
		return 1./(1. + 1j*(f/self.f0)**self.order)
			

	def _filter(self, f): # Butterworth
		return 1./np.sqrt(1 + (f/self.f0)**(2*self.order))


class HighPassFilter(Filter):
	def __init__(self, l0, order=1):
		self.f0 = c/l0
		self.order = order

	def _filter_old(self, f): # Non causal
		return 1./(1. + 1j*(self.f0/f)**self.order)

	def _filter(self, f): # Butterworth
		return 1./np.sqrt(1 + (self.f0/f)**(2*self.order))


class BandPassFilter(Filter):
	def __init__(self, l0, dl, order=1):
		self.low_pass = LowPassFilter(l0 - 0.5*dl, order)
		self.high_pass = HighPassFilter(l0 + 0.5*dl, order)

	def _filter(self, f):
		return self.low_pass._filter(f) * self.high_pass._filter(f)


class LorentzianFilter(Filter):
	def __init__(self, l0, dl):
		self.f0 = c/l0
		self.df = c * dl / l0**2

	
	def _filter(self, f):
		return tools.complex_lorentzian(f, self.f0, self.df)


class DielectricFilter(Filter):
	def __init__(self, dielectric_mirror, angle=0.):
		self.dielectric_mirror = dielectric_mirror
		self.angle = angle
	
	def _filter_x(self, f):
		return self.dielectric_mirror.transmission(c/f, self.angle, pol='p')

	def _filter_y(self, f):
		return self.dielectric_mirror.transmission(c/f, self.angle, pol='s')


########################## BEAM SPLITTERS ###################################################

class BeamSplitter(object):
	def __init__(self, T1=1, T2=None):
		if T2 is None:
			T2 = 1 - T1
		
		self.T1 = T1
		self.T2 = T2

		self.path_1 = TransmissionLoss(self.T1)
		self.path_2 = TransmissionLoss(self.T2)


	def __call__(self, *beams, **kwargs):
		self.output_1 = self.path_1(*beams, **kwargs)
		self.output_2 = self.path_2(*beams, **kwargs)

		return self.output_1


class DichroicBeamSplitter(BeamSplitter):
	def __init__(self, l0, order=1, main_path='low_pass'):
		self.l0 = l0
		self.order = order

		if main_path == 'low_pass':
			self.path_1 = LowPassFilter(l0, order)
			self.path_2 = HighPassFilter(l0, order)
		elif main_path == 'high_pass':
			self.path_1 = HighPassFilter(l0, order)
			self.path_2 = LowPassFilter(l0, order)
		else:
			raise AttributeError("main_path must be either 'low_pass' or 'high_pass'")


class PolarizingBeamSplitter(BeamSplitter):
	def __init__(self, angle=0., T1=1, T2=1, t1=0, t2=0):
		self.angle = angle
		self.T1, self.T2 = T1, T2
		self.t1, self.t2 = t1, t2

		self.path_1 = Polarizer(angle, T1, t1)
		self.path_2 = Polarizer(angle + 0.5*pi, T2, t2)






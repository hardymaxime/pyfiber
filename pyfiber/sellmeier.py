# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

# Importation des librairies de python nécessaires à la résolution du problème
import numpy as np
from pkg_resources import resource_stream as fopen
from scipy.interpolate import interp1d
from scipy.constants import pi,c

from . import tools

# Note : pour ce code, la variable l correspond à la longueur d'onde

# Coefficients de Sellmeier
A_Sm = dict(SiO2=[0.696166,0.407942,0.897479], GeO2=[0.806866,0.718156,0.854168], Al2O3=[1.023798,1.058264,5.280792])
l_Sm = dict(SiO2=[0.068404e-6,0.116241e-6,9.896161e-6], GeO2=[0.068972e-6,0.153966e-6,11.84193e-6], Al2O3=[0.06144821e-6,0.1106997e-6,17.92656e-6])

# Al2O3: I. H. Malitson, Refraction and Dispersion of Synthetic Sapphire, Journal of the Optical Society of America, Vol. 52, Num. 12, 1962.

class Index(object):
	def __init__(self, verre=dict(SiO2=1.)):
		'''verre est un dictionnaire. Chaque element composant le verre est associe avec sa proportion
		Par defaut, il s'agit d'un verre de SiO2'''

		self.verre = verre

		self.A = [0.,0.,0.]
		self.l = [0.,0.,0.]

		for i in range(0,3):
			for element in self.verre:
				self.A[i] += A_Sm[element][i] * verre[element]
				self.l[i] += l_Sm[element][i] * verre[element]


	def __call__(self, l):
		'''Calcule l'indice de refraction en fonction de la longueur d'onde 'l' '''

		s = 0.

		for i in range(0,3):
			s += (self.A[i] * l**2.)/(l**2. - (self.l[i])**2.)

		# Indice de refraction
		return np.sqrt(s+1.)


class StepIndexBeta(object):
	def __init__(self, n_core=Index(), n_clad=Index(), core_radius=1., l_m=(0,1)):
		self.n_core = n_core
		self.n_clad = n_clad
		self.core_radius = core_radius

		tab_u_V = np.load(fopen('pyfiber','data/LP_{}_{}.npy'.format(*l_m)))
		self.func_u_V = interp1d(tab_u_V[0],tab_u_V[1])


	def __call__(self, l):
		n1 = self.n_core(l)
		n2 = self.n_clad(l)
		V = 2*pi*self.core_radius*np.sqrt(n1**2 - n2**2)/l

		u = self.func_u_V(V)
		w = np.sqrt(V**2 - u**2)

		return 2*pi*np.sqrt((n1*w/V)**2 + (n2*u/V)**2)/l

	
	def beta1(self, l):
		return (-l**2/(2*pi*c)) * np.gradient(self.__call__(l)) / abs(l[1] - l[0])

	def beta2(self, l):
		return (-l**2/(2*pi*c)) * np.gradient(self.beta1(l)) / abs(l[1] - l[0])




		

	

		


## Désuet
## Fonction calculant l'indice de réfraction pour un verre de SiO2 et de GeO2 (selon la fraction molaire x de GeO2)
#def test(L,x):
#	'''Calcule l'indice de refraction 'n' d'apres la longueur d'onde 'L'
#	et la fraction molaire 'x' de GeO2 dans le SiO2.'''
#
#	s = 0	# variable contenant la somme
#	A_Si = A_Sm['SiO2']
#	L_Si = l_Sm['SiO2']
#	A_Ge = A_Sm['GeO2']
#	L_Ge = l_Sm['GeO2']
#	
#	# Calcul de la somme
#	for i in range(0,3):
#		s += ((A_Si[i] + x*(A_Ge[i]-A_Si[i]))*L**2.)/(L**2. - (L_Si[i] + x*(L_Ge[i] - L_Si[i]))**2.)
#
#	# Indice de réfraction
#	return sqrt(s+1)


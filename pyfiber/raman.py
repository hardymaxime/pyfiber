# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

from scipy.constants import hbar, c, pi
import numpy as np

from . import evolution, tools, beam

# Profil du gain Raman de la silice selon Hollenbeck2002
_tab = np.loadtxt(tools.open_local_file('data/raman_profile.txt'),delimiter='\t').T
_tab[1] *= 2*pi*c * 1e2	# Amplitude
_tab[2] *= 2*pi*c * 1e2 / (4 * np.sqrt(np.log(2))) # Gaussian FWHM
_tab[3] *= 2*pi*c * 1e2 # Lorentzian FWHM

def _hR_SiO2_old(w):
	res = np.zeros(len(w),dtype='complex64') if hasattr(w,'len') else 0.
	for i in range(len(_tab.T)):
		res += 1j*_tab[0,i] * tools.voigt(w,_tab[1,i],_tab[2,i],_tab[3,i])
		res += -1j*_tab[0,i] * tools.voigt(w,-_tab[1,i],_tab[2,i],_tab[3,i])

	res *= 1.38/2.778e-12
	return res


def _hR_SiO2_approx_old(w):
	tau1 = 12.2e-15
	tau2 = 32e-15
	A = (tau1**2. + tau2**2.)/(2j*np.sqrt(2*pi)*tau1*tau2)

	return 2.38*A*( (1/(1-1j*tau2*(w+1/tau1))) - (1/(1-1j*tau2*(w-1/tau1))) )

def _hR_SiO2_approx(w):
	tau1 = 12.2e-15
	tau2 = 32e-15
	A = (tau1**2. + tau2**2.)/(2j*np.sqrt(2*pi)*tau1*tau2)

	return A*( (1/(1-1j*tau2*(w+1/tau1))) - (1/(1-1j*tau2*(w-1/tau1))) )



def _hR_SiO2(w):
	res = np.zeros(len(w),dtype='complex64') if hasattr(w,'len') else 0.
	for i in range(len(_tab.T)):
		res += 1j*_tab[0,i] * tools.voigt(w,_tab[1,i],_tab[2,i],_tab[3,i])
		res += -1j*_tab[0,i] * tools.voigt(w,-_tab[1,i],_tab[2,i],_tab[3,i])

	res /= 2.24343e-12 * np.sqrt(2*pi)
	return res




# Profil du gain Raman du ZBLAN selon Yan2012
_tabz = np.loadtxt(tools.open_local_file('data/raman_profile_zblan.txt'),delimiter='\t').T
_tabz[1] *= 2*pi*c * 1e2
_tabz[2] *= 2*pi*c * 1e2 / (4 * np.sqrt(np.log(2)))
_tabz[3] *= 2*pi*c * 1e2

def _hR_ZBLAN(w):
	res = np.zeros(len(w),dtype='complex64') if hasattr(w,'len') else 0.
	for i in range(len(_tabz.T)):
		res += 1j*_tabz[0,i] * tools.voigt(w,_tabz[1,i],_tabz[2,i],_tabz[3,i])
		res += -1j*_tabz[0,i] * tools.voigt(w,-_tabz[1,i],_tabz[2,i],_tabz[3,i])

	res *= 2.3758e11 
	return res


# Dictionary containing Raman hR functions for different materials
hR = {}
hR['SiO2_old'] = _hR_SiO2_old
hR['SiO2_approx'] = _hR_SiO2_approx
hR['SiO2_approx_old'] = _hR_SiO2_approx_old
hR['SiO2'] = _hR_SiO2
hR['ZBLAN'] = _hR_ZBLAN



# Useful tools for Raman amplification

def pump_wavelength(signal_wavelength, delta_nu=13e12):
	return signal_wavelength/(1. + delta_nu*signal_wavelength/c)


def raman_length(final_ratio, signal, pump, fiber, T=0, material='SiO2', pol='x'):
	#gR = 0.18j * fiber.gamma(signal) * np.sqrt(2*pi) * hR[material](signal.w0 - pump.w0)
	i = np.argmin(np.abs(signal.T-T))
	gR = 0.18j * fiber.gamma(signal) * np.sqrt(2*pi) * hR[material](signal.chirp(pol=pol)[i] + signal.w0 - pump.chirp(pol=pol)[i] - pump.w0)
	Ps0, Pp0 = signal.Pmax(), pump.Pmax()
	P = Pp0 + Ps0
	Ps, Pp = final_ratio * P, (1-final_ratio) * P

	return (1/(2*gR.real*P)) * np.log( (Pp0*Ps) / (Pp * Ps0) )







# Evolution classes

class SelfRamanEvolution(evolution.BeamEvolution):
	def _NLS_time(self, dz):
		for each_beam in self.pulse_list:
			self._phase[each_beam] += self.self_raman_gain(dz, each_beam)

		super()._NLS_time(dz)
		

	def self_raman_gain(self, dz, the_beam):
		P0, P1 = np.abs(the_beam.A[0])**2, np.abs(the_beam.A[1])**2

		dP0 = tools.derivative(P0)/the_beam.dT # dP/dT
		dP1 = tools.derivative(P1)/the_beam.dT
		dAA = tools.derivative(the_beam.A[0]*np.conj(the_beam.A[1]))/the_beam.dT # d((A+).(A-)*)/dT

		phase = np.zeros(the_beam.A.shape, dtype='complex64')
		phase[0] = -2j*self.fiber.gamma(the_beam)*dz*self.fiber.TR * (dP0 + the_beam.A[1]*(dAA/the_beam.A[0]))/3
		phase[1] = -2j*self.fiber.gamma(the_beam)*dz*self.fiber.TR * (dP0 + the_beam.A[0]*(np.conj(dAA)/the_beam.A[1]))/3

		return np.nan_to_num(phase)



class PicoSecondRamanAmplification(evolution.BeamEvolution):
	def _NLS_time(self, dz): 
		for each_beam in self.pulse_list:
			for other_beam in (b for b in self.pulse_list if b is not each_beam):
				self._phase[each_beam] += self.raman_gain(dz, each_beam, other_beam)

		super()._NLS_time(dz)
	


	def raman_gain(self, dz, the_beam, other_beam):
		sign = 1 if the_beam.l0 > other_beam.l0 else -1
		gain = np.zeros(the_beam.A.shape, dtype='complex64')

		gain[0] = sign * 0.25 * self.fiber.gamma(the_beam) * other_beam.P() * dz
		gain[1] = gain[0]

		return gain

	
	def cross_phase_modulation(self, dz, the_beam, other_beam):
		return (1-0.18) * super().cross_phase_modulation(dz, the_beam, other_beam)
	






class RamanAmplification(evolution.BeamEvolution):
	'''
	Amplification by Stimulated Raman scattering in the almost exact approximation of 13 peaks consisting
	of a convolution of a gaussian and a lorentzian (Voigt profile).
	It considers only the stimulated Raman scattering of 'x' polarization. Therefore, the 'y' polarization of the pulses must be null.
	'''
	def __init__(self, epsilon=1e-10, fftw=['FFTW_MEASURE'], material='SiO2', fR=0.18, **kwargs):
		self.epsilon = epsilon
		self._fftw_flags = fftw
		self.material = material
		self.fR = fR

		# Evaluation of the Raman gain and phase 
		self._hR_w = {}
		self._hR_w_sp = {}

		super().__init__(**kwargs)



	def _NLS_init(self):
		super()._NLS_init()

		self.FFTW = tools.FFTW(np.zeros(self.beam_list[0].Npts, dtype='complex64'), fftw_flags=self._fftw_flags)

	

	def _NLS_raman(self, dz):
		for each_beam in self.pulse_list:
			self._phase[each_beam] += self.raman_phase_modulation(dz, each_beam)
			
			for other_beam in (b for b in self.pulse_list if b is not each_beam):
				self._phase[each_beam] += self.raman_gain(dz, each_beam, other_beam)

		
	def _NLS_time(self, dz):
		self._NLS_raman(dz)
		super()._NLS_time(dz)


	def raman_phase_modulation(self, dz, the_beam):
		if the_beam not in self._hR_w:
			self._hR_w[the_beam] = np.fft.ifftshift(hR[self.material](the_beam.w))

		phase = np.zeros(the_beam.A.shape, dtype='complex64')

		# Ptot
		for each_beam in self.beam_list:
			phase[0] += each_beam.P()

		# Autocorr
		phase[0] = np.sqrt(2*pi) * self.FFTW.fftw(self._hR_w[the_beam] * self.FFTW.ifftw(phase[0]))
		phase[1] = phase[0]

		return 1j * self.fiber.gamma(the_beam) * self.fR * dz * phase
		

	
	def raman_gain(self, dz, the_beam, other_beam):
		if (the_beam,other_beam) not in self._hR_w_sp:
			# WR change its sign if each_beam and each_pump are counter propagating
			#WR = self.sign[each_beam] * self.sign[each_pump] * (each_pump.w0 - each_beam.w0)
			WR = other_beam.w0 - the_beam.w0
			self._hR_w_sp[the_beam,other_beam] = np.fft.ifftshift(hR[self.material](the_beam.w-WR))

		if not hasattr(self, 'FFTW'):
			self.FFTW = tools.FFTW(np.zeros(the_beam.Npts, dtype='complex64'), fftw_flags=self._fftw_flags)
			

		gain = np.zeros(the_beam.A.shape, dtype='complex64')

		# A_i A_j^*
		gain[0] = 2 * the_beam.A[0] * np.conj(other_beam.A[0])

		# Cross-correlation
		gain[0] = np.sqrt(2*pi) * other_beam.A[0] * self.FFTW.fftw(self._hR_w_sp[the_beam,other_beam] * self.FFTW.ifftw(gain[0]))
		gain[0] /= (the_beam.A[0] + self.epsilon)

		gain[1] = gain[0]

		return 1j * self.fiber.gamma(the_beam) * self.fR * dz * gain
	


	def self_phase_modulation(self, dz, the_beam):
		return (1 - self.fR) * super().self_phase_modulation(dz, the_beam)	


	def cross_phase_modulation(self, dz, the_beam, other_beam):
		return (1 - self.fR) * super().cross_phase_modulation(dz, the_beam, other_beam)


	

	# Obsolete	
	def _Raman_gain_spectrum(self, signal, pump):
		signal.At += (1.+1j)*self.epsilon * np.sqrt(2) / signal.Dw()

		gain = signal.dT*signal.Npts * np.fft.ifftshift(np.fft.ifft(signal.A[0]*self.raman_gain(1,signal,pump)[0]) /\
				(np.sqrt(2*np.pi)*signal.At[0]))

		signal.At -= (1.+1j)*self.epsilon * np.sqrt(2) / signal.Dw()

		return gain





class PolarizedRamanAmplification(RamanAmplification):
	def raman_phase_modulation(self, dz, the_beam):
		# Pourquoi est-il identique à sans les polarisations ???
		if the_beam not in self._hR_w:
			self._hR_w[the_beam] = np.fft.ifftshift(hR[self.material](the_beam.w))

		phase = np.zeros(the_beam.A.shape, dtype='complex64')

		# Ptot
		for each_beam in self.beam_list:
			phase[0] += each_beam.P()

		# Autocorr
		phase[0] = np.sqrt(2*pi) * self.FFTW.fftw(self._hR_w[the_beam] * self.FFTW.ifftw(phase[0]))
		phase[1] = phase[0]

		return 1j * self.fiber.gamma(the_beam) * self.fR * dz * phase
		
		
	def raman_gain(self, dz, the_beam, other_beam):
		if (the_beam,other_beam) not in self._hR_w_sp:
			# WR change its sign if each_beam and each_pump are counter propagating
			#WR = self.sign[each_beam] * self.sign[each_pump] * (each_pump.w0 - each_beam.w0)
			WR = other_beam.w0 - the_beam.w0
			self._hR_w_sp[the_beam,other_beam] = np.fft.ifftshift(hR[self.material](the_beam.w-WR))


		gain = np.zeros(the_beam.A.shape, dtype='complex64')

		# A_i A_j^*
		gain[0] = the_beam.A[0] * np.conj(other_beam.A[0]) + the_beam.A[1] * np.conj(other_beam.A[1]) # scalar product

		# Cross-correlation
		gain[0] = np.sqrt(2*pi) * self.FFTW.fftw(self._hR_w_sp[the_beam,other_beam] * self.FFTW.ifftw(gain[0]))
		gain[1] = gain[0]

		gain[0] *= other_beam.A[0] / (the_beam.A[0] + self.epsilon)
		gain[1] *= other_beam.A[1] / (the_beam.A[1] + self.epsilon)

		return 1j * self.fiber.gamma(the_beam) * self.fR * dz * gain




class AnalyticRamanAmplification(object):
	def __init__(self, fiber, length=1., Nsaves=100, progress=False, material='SiO2'):
		self.fiber = fiber
		self.length = length
		self.Nsaves = Nsaves
		self.dz = self.length / self.Nsaves
		self.material = material
		self.progress = progress


	def raman_length(self, signal, pump, final_ratio=0.5, T=0, pol='x'):
		gR = self.raman_gain(signal,pump,T)
		Fs0, Fp0 = signal.Pmax()/self.fiber.gamma(signal), pump.Pmax()/self.fiber.gamma(pump)
		F = Fp0 + Fs0
		Fs, Fp = final_ratio * F, (1-final_ratio) * F
	
		return (1/(2*self.fiber.gamma(signal)*self.fiber.gamma(pump)*gR.real*F)) * np.log( (Fp0*Fs) / (Fp * Fs0) )


	def raman_gain(self, signal, pump, T=None, fit_chirps=None):
		if T is None:	
			hR_T = hR[self.material](signal.chirp(pol='x',fit=fit_chirps) + signal.w0 -\
					pump.chirp(pol='x',fit=fit_chirps) - pump.w0)
		else:
			i = np.argmin(np.abs(signal.T-T))
			hR_T = hR[self.material](signal.chirp(pol='x',fit=fit_chirps)[i] + signal.w0 -\
					pump.chirp(pol='x',fit=fit_chirps)[i] - pump.w0)
		return 0.18j * np.sqrt(2*pi) * hR_T

	
	def R(self, signal, pump, T=0, gR=None): # Has to change to take T=0 into account instead of max
		Gs, Gp = self.fiber.gamma(signal), self.fiber.gamma(pump)
		Fs0, Fp0 = signal.Pmax()/Gs, pump.Pmax()/Gp
		F = signal.P()/Gs + pump.P()/Gp
		gR = self.raman_gain(signal,pump) if gR is None else gR
		gR0 = self.raman_gain(signal,pump,T)
		return (Fp0 / Fs0)**((gR.real*F)/(gR0.real*np.amax(F)))
		

	def signal_power(self, signal, pump, z):
		Gs, Gp = self.fiber.gamma(signal), self.fiber.gamma(pump)
		L_R = self.raman_length(signal, pump)
		R = self.R(signal, pump)
		Fs0, Fp0 = signal.P()/Gs, pump.P()/Gp
		F = Fp0 + Fs0
		return Gs * F * Fs0 / (Fp0 * R**(-z/L_R) + Fs0)


	def pump_power(self, signal, pump, z):
		Gs, Gp = self.fiber.gamma(signal), self.fiber.gamma(pump)
		L_R = self.raman_length(signal, pump)
		R = self.R(signal, pump)
		Fs0, Fp0 = signal.P()/Gs, pump.P()/Gp
		F = Fp0 + Fs0
		return Gp * F * Fp0 / (Fp0 + Fs0 * R**(z/L_R))


	def signal_phase(self, signal, pump, z, fit_chirps=None):
		Gs, Gp = self.fiber.gamma(signal), self.fiber.gamma(pump)
		gR = self.raman_gain(signal, pump)
		Fs0, Fp0 = signal.P()/Gs, pump.P()/Gp
		Fs, Fp = self.signal_power(signal, pump, z)/Gs, self.pump_power(signal, pump, z)/Gp
		return (1/(2*gR.real)) * ((gR.imag+1.82)*np.log(Fs/Fs0) - (Gs/Gp)*np.log(Fp/Fp0))


	def pump_phase(self, signal, pump, z):
		Gs, Gp = self.fiber.gamma(signal), self.fiber.gamma(pump)
		gR = self.raman_gain(signal,pump)
		Fs0, Fp0 = signal.P()/Gs, pump.P()/Gp
		Fs, Fp = self.signal_power(signal, pump, z)/Gs, self.pump_power(signal, pump, z)/Gp
		return (1/(2*gR.real)) * (-(gR.imag+1.82)*np.log(Fp/Fp0) + (Gp/Gs)*np.log(Fs/Fs0))


	def __call__(self, signal, pump, T=0, *ignored, fit_chirps=None, **kwargs):
		if self.progress:
			self.progress_bar = tools.ProgressBar(self.Nsaves, self.progress)

		self.beam_list = beam.BeamList([signal,pump]).copy(Nsaves=self.Nsaves, **kwargs)
		self.signal = self.beam_list[0]
		self.pump = self.beam_list[1]
		
		# Préparation pour l'évolution
		Gs, Gp = self.fiber.gamma(self.signal), self.fiber.gamma(self.pump)
		Fs0, Fp0 = self.signal.P()/Gs, self.pump.P()/Gp
		F = Fs0 + Fp0
		gR = self.raman_gain(self.signal, self.pump, fit_chirps=fit_chirps)
		R = self.R(self.signal, self.pump, T, gR)
		L_R = self.raman_length(self.signal, self.pump, T=T)

		phis0 = np.unwrap(np.angle(self.signal.A[0]))
		phip0 = np.unwrap(np.angle(self.pump.A[0]))
		
		# Evolution de l'impulsion
		for z in np.linspace(0, self.length, self.Nsaves):
			Fp = F * Fp0 / (Fp0 + Fs0*R**(z/L_R))
			Fs = F - Fp

			phis = phis0 + (1/(2*gR.real))*((gR.imag+1.82)*np.log(Fs/Fs0) - (Gs/Gp)*np.log(Fp/Fp0))
			phip = phip0 - (1/(2*gR.real))*((gR.imag+1.82)*np.log(Fp/Fp0) - (Gp/Gs)*np.log(Fs/Fs0))

			As = np.nan_to_num(np.sqrt(0.5 * Gs * Fs) * np.exp(1j * phis))
			Ap = np.nan_to_num(np.sqrt(0.5 * Gp * Fp) * np.exp(1j * phip))
			self.signal.A.set(np.array([As, As]))
			self.pump.A.set(np.array([Ap, Ap]))

			for each_beam in self.beam_list:
				each_beam.z.set(z)
				each_beam.update_At()
				each_beam.save()
			
			if self.progress:
				self.progress_bar.increment()

		if self.progress:
			self.progress_bar.increment()

		return self.beam_list



class ExactAnalyticRamanAmplification(AnalyticRamanAmplification):
	# Rétrocompatibilité
	pass


class TestAnalyticRamanAmplification(ExactAnalyticRamanAmplification):
	def __call__(self, signal, pump, T=0, *ignored, **kwargs):
		if self.progress:
			self.progress_bar = tools.ProgressBar(self.Nsaves, self.progress)

		self.beam_list = beam.BeamList([signal,pump]).copy(Nsaves=self.Nsaves, **kwargs)
		self.signal = self.beam_list[0]
		self.pump = self.beam_list[1]
		
		# Préparation pour l'évolution
		Gs, Gp = self.fiber.gamma(self.signal), self.fiber.gamma(self.pump)
		Fs0, Fp0 = self.signal.P()/Gs, self.pump.P()/Gp
		F = Fs0 + Fp0
		gR = self.raman_gain(self.signal, self.pump)
		R = self.R(self.signal, self.pump, T, gR)
		L_R = self.raman_length(self.signal, self.pump, T=T)

		phis0 = np.unwrap(np.angle(self.signal.A[0]))
		phip0 = np.unwrap(np.angle(self.pump.A[0]))
		
		# Evolution de l'impulsion
		for z in np.linspace(0, self.length, self.Nsaves):
			Fs = Fs0 * R**(z/L_R)
			Fp = F - Fs

			phis = phis0 + (1/(2*gR.real))*((gR.imag+1.82)*np.log(Fs/Fs0) - np.log(Fp/Fp0))
			phip = phip0 - (1/(2*gR.real))*((gR.imag+1.82)*np.log(Fp/Fp0) - np.log(Fs/Fs0))

			As = np.nan_to_num(np.sqrt(0.5 * Gs * Fs) * np.exp(1j * phis))
			Ap = np.nan_to_num(np.sqrt(0.5 * Gp * Fp) * np.exp(1j * phip))
			self.signal.A.set(np.array([As, As]))
			self.pump.A.set(np.array([Ap, Ap]))

			for each_beam in self.beam_list:
				each_beam.z.set(z)
				each_beam.update_At()
				each_beam.save()
			
			if self.progress:
				self.progress_bar.increment()

		if self.progress:
			self.progress_bar.increment()

		return self.beam_list




class OldAnalyticRamanAmplification(object):
	def __init__(self, fiber, length=1., Nsaves=100, material='SiO2'):
		self.fiber = fiber
		self.length = length
		self.Nsaves = Nsaves
		self.dz = self.length / self.Nsaves
		self.material = material


	def raman_gain(self, signal, pump, T=None):
		if T is None:	
			hR_T = hR[self.material](signal.chirp(pol='x') + signal.w0 - pump.chirp(pol='x') - pump.w0)
		else:
			i = np.argmin(np.abs(signal.T-T))
			hR_T = hR[self.material](signal.chirp(pol='x')[i] + signal.w0 - pump.chirp(pol='x')[i] - pump.w0)
		return 0.18j * self.fiber.gamma(signal) * np.sqrt(2*pi) * hR_T


	def signal_power(self, signal, pump, z, normalized=False):
		Ps0, Pp0 = signal.P(), pump.P()
		P = Ps0 + Pp0
		gain = np.exp(2* self.raman_gain(signal,pump).real * P * z)
		Ps = np.nan_to_num(P * (Ps0 * gain) / (Pp0 + Ps0 * gain))
		return Ps / np.amax(Ps) if normalized else Ps



	def pump_power(self, signal, pump, z, normalized=False):
		Ps0, Pp0 = signal.P(), pump.P()
		P = Ps0 + Pp0
		return P * Pp0 / (Pp0 + Ps0 * np.exp(2 * self.raman_gain(signal,pump) * P *z))
		
	
	def signal_phase(self, signal, pump, z):
		gR = self.raman_gain(signal,pump)
		Ps0, Pp0 = signal.P(), pump.P()
		P = Ps0 + Pp0
		return np.nan_to_num((gR.imag + 1.82*self.fiber.gamma(signal)) * P * z -\
				(0.5*(gR.imag+0.82*self.fiber.gamma(signal))/gR.real) *\
				np.log((Ps0*np.exp(2*gR.real*P*z) + Pp0) / P))

	def R(self, signal, pump):
		Ps0, Pp0 = signal.Pmax(), pump.Pmax()
		P = signal.P() + pump.P()
		hR_T = hR[self.material](signal.chirp(pol='x') + signal.w0 - pump.chirp(pol='x') - pump.w0)
		hR_T0 = hR[self.material](signal.w0-pump.w0)
		return (3 * Pp0 / Ps0)**((hR_T.imag*P)/(hR_T0.imag*np.amax(P)))


	def pump_phase(self, signal, pump, z):
		gR = self.raman_gain(signal,pump)
		Ps0, Pp0 = signal.P(), pump.P()
		P = Ps0 + Pp0
		return self.fiber.gamma(pump) * P * z +\
				(0.5*(gR.imag+self.fiber.gamma(pump))/gR.real) *\
				np.log((Ps0*np.exp(2*gR.real*P*z) + Pp0) / P)


	def __call__(self, signal, pump, *ignored, **kwargs):
		self.beam_list = beam.BeamList([signal,pump]).copy(Nsaves=self.Nsaves, **kwargs)
		self.signal = self.beam_list[0]
		self.pump = self.beam_list[1]
		#self.chirp = chirp

		#for each_beam in self.beam_list:
		#	if each_beam not in self.chirp:
		#		self.chirp[each_beam] = each_beam.C()

		# Détermination du gain Raman
		#DT_s, DT_p = self.signal.DT(), self.pump.DT()
		#C_sp = (self.chirp[signal] * DT_p**2 - self.chirp[pump] * DT_s**2) / (DT_p**2 + DT_s**2)
		#DT_sp = (DT_s * DT_p) / np.sqrt(DT_s**2 + DT_p**2)
		#w_R = self.pump.w0 - self.signal.w0

		#self.hR_T = hR['SiO2']((2*C_sp*self.signal.T)/DT_sp**2 - w_R)
		#self.hR_T = hR[self.material](self.signal.chirp(pol='x') + self.signal.w0 - self.pump.chirp(pol='x') - self.pump.w0)
		#self.gR = 0.18j * self.fiber.gamma(self.signal) * np.sqrt(2*pi) * self.hR_T
		self.gR = self.raman_gain(self.signal, self.pump)

		self.gamma = {}
		for each_beam in self.beam_list:
			self.gamma[each_beam] = self.fiber.gamma(each_beam)

		
		# Préparation pour l'évolution
		Ps0, Pp0 = self.signal.P(), self.pump.P()
		P = Ps0 + Pp0

		#phis0 = - self.chirp[signal] * (self.signal.T / self.signal.DT())**2
		#phip0 = - self.chirp[pump] * (self.pump.T / self.pump.DT())**2
		phis0 = np.unwrap(np.angle(self.signal.A[0]))
		phip0 = np.unwrap(np.angle(self.pump.A[0]))
		
		# Evolution de l'impulsion
		for z in np.linspace(0, self.length, self.Nsaves):
			Pp = P * Pp0 / (Pp0 + Ps0 * np.exp(2 * self.gR.real * P *z))	# should be faster
			Ps = P - Pp

			phip = phip0 + self.gamma[self.pump] * P * z +\
					(0.5*(self.gR.imag+self.gamma[self.pump])/self.gR.real) *\
					np.log((Ps0*np.exp(2*self.gR.real*P*z) + Pp0) / P)

			phis = phis0 + (self.gR.imag + 1.82*self.gamma[self.signal]) * P * z -\
					(0.5*(self.gR.imag+0.82*self.gamma[self.signal])/self.gR.real) *\
					np.log((Ps0*np.exp(2*self.gR.real*P*z) + Pp0) / P)

			#phis = phis0 + (self.gR.imag + 1.82*self.gamma[self.signal]) * P * z 
			#phip = phip0 + self.gamma[self.pump] * P * z

			#phis = phis0
			#phip = phip0

			#phip = phip0 + (0.5*(self.gR.imag+self.gamma[self.pump])/self.gR.real) *\
			#		np.log((Ps0*np.exp(2*self.gR.real*P*z) + Pp0) / P)

			#phis = phis0 - (0.5*(self.gR.imag+0.82*self.gamma[self.signal])/self.gR.real) *\
			#		np.log((Ps0*np.exp(2*self.gR.real*P*z) + Pp0) / P)
			
			As = np.nan_to_num(np.sqrt(0.5 * Ps) * np.exp(1j * phis))
			Ap = np.nan_to_num(np.sqrt(0.5 * Pp) * np.exp(1j * phip))
			self.signal.A.set(np.array([As, As]))
			self.pump.A.set(np.array([Ap, Ap]))

			for each_beam in self.beam_list:
				each_beam.z.set(z)
				each_beam.update_At()
				each_beam.save()

		return self.beam_list







		
	
			



		



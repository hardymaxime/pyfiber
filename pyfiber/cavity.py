# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import numpy as np
from scipy.constants import hbar, c, pi

from . import tools, beam, component

class Cavity(object):
	def __init__(self, *components):
		self.components = components


	def start(self, *beams, N=50, step=5):
		# Start the laser with noise
		self.beam_list = [None for i in range(len(self.components))]
		self.beam_list[0] = beam.BeamList(beams)

		pg = tools.ProgressBar(N, step)

		for i in range(N):
			for j in range(len(self.components)):
				if j == len(self.components) - 1:
					self.beam_list[0] = self.components[j](*self.beam_list[j], z0=0)
				else:
					self.beam_list[j+1] = self.components[j](*self.beam_list[j], z0=self.beam_list[j][0].z())

			pg.increment()
		
		pg.increment()
		

		
	def get_beam(self, i=0):
		return [self.beam_list[j][i] for j in range(len(self.beam_list))]


		



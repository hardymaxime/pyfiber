# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

from scipy.constants import hbar, h, c, pi
import numpy as np

from . import evolution, tools, beam, raman, dopant


class LorentzianAmplification(evolution.BeamEvolution):
	def __init__(self, g0, l0, dl, Esat, **kwargs):
		self.g0 = g0
		self.w0 = 2*pi*c/l0
		self.dw = dl * 2*pi*c/l0**2
		self.Esat = Esat

		super().__init__(**kwargs)


	def gain_function(self, w):
		return self.g0 / (1 + 4 * ((w - self.w0)/self.dw)**2)


	def gain(self, dz, the_beam):
		if the_beam not in self._gain:
			self._gain[the_beam] = self.gain_function(the_beam.w + the_beam.w0)

		gain = np.zeros(the_beam.A.shape, dtype='complex64')

		gain[0] += 0.5 * dz * self._gain[the_beam] / (1 + the_beam.energy() / self.Esat)
		gain[1] = gain[0]

		return gain


	def _NLS_init(self):
		super()._NLS_init()
		self._gain = {}


	def _NLS_freq(self, dz):
		for each_beam in self.beam_list:
			self._phase[each_beam] += self.gain(dz, each_beam)

		super()._NLS_freq(dz)




class ComplexLorentzianAmplification(LorentzianAmplification):
	def gain_function(self, w):
		return self.g0 / (1 + 2j * ((w - self.w0)/self.dw) )


class QuickAndDirtyAmplification(object):
	def __init__(self, g0, l0, dl, Esat):
		self.g0 = g0
		self.w0 = 2*pi*c/l0
		self.dw = dl * 2*pi*c/l0**2
		self.Esat = Esat

	def __call__(self, *beams, **kwargs):
		beams = beam.BeamList(beams).copy(**kwargs)

		for each_beam in beams:
			gain = np.zeros(each_beam.At.shape, dtype='complex64')
			gain[0] = 0.5 * self.g0 /\
					( (1 + 2j * ((each_beam.w + each_beam.w0 - self.w0)/self.dw)) * (1 + each_beam.energy()/self.Esat) )
			gain[1] = gain[0]

			each_beam.freq_gain(gain)

		return beams




class TwoLevelApproxAmplification(evolution.BeamEvolution):
	def __init__(self, pump, **kwargs):
		super().__init__(**kwargs)

		self.pump = pump
		self.fiber.set_mode_attributes(pump.l0, pump.mode)

		for dopant in self.fiber.dopants.values():
			if isinstance(dopant,TwoLevelDopant):
				self.dopant = dopant
				break

		if not hasattr(self,'dopant'):
			raise AttributeError('Given fiber has no Two-level dopant')

		self.Pth = np.real((pi * self.fiber.core_radius**2 * hbar * self.pump.w0) \
				/ (self.pump.mode.confinement * self.dopant.tau * self.dopant.sigma12(self.pump.l0)))


	def _NLS_init(self):
		super()._NLS_init()

		self._gain = {}
		self._Psat = {}

		self._Pinit = self.pump.P()

		for each_beam in self.beam_list:
			self.Pinit += each_beam.Pmean()


	def gain(self, dz, the_beam):
		if the_beam not in self._gain:
			self.gain[each_beam] = 0.5 * self.dopant.N * each_beam.mode.confinement \
					* (self.dopant.sigma21(each_beam.l) - self.dopant.sigma12(each_beam.l))

		if the_beam not in self._Psat:
			self.Psat[each_beam] = np.real((2 * pi * self.fiber.core_radius**2 * hbar * each_beam.w0) \
					/ (each_beam.mode.confinement * self.dopant.tau \
					* (self.dopant.sigma12(each_beam.l0) + self.dopant.sigma21(each_beam.l0))))

		P = 0.
		for each_beam in self.beam_list:
			P += each_beam.Pmean()

		Pp = self._Pinit - P

		gain = np.zeros(the_beam.A.shape, dtype='complex64')
		gain[0] += 0.5 * dz * self._gain[the_beam] / (1 + the_beam.Pmean() * self.Pth / (Pp * self._Psat[the_beam]))
		gain[1] = gain[0]

		return gain


	def _NLS_freq(self, dz):
		for each_beam in self.beam_list:
			self._phase[each_beam] += self.gain(dz, the_beam)

		super()._NLS_freq(self, dz)



class RareEarthAmplification(evolution.BeamEvolution):
	def _NLS_init(self):
		super()._NLS_init()

		self.dopants = {}
		Npts = len(self.beam_list[0].T) if hasattr(self.beam_list[0], 'T') else 1

		for each_dopant in self.fiber.dopants.values():
			self.dopants[each_dopant.symbol] = each_dopant.copy(Nsaves=self.Nsaves, Npts=Npts)

		self.find_populations()


	def _NLS_save(self, dz):
		super()._NLS_save(dz)

		for each_dopant in self.dopants.values():
			each_dopant.save()


	def _NLS_loop_init(self, dz):
		super()._NLS_loop_init(dz)

		for each_dopant in self.dopants.values():
			each_dopant.z += dz

		self.find_populations()


	def find_populations(self):
		for each_dopant in self.dopants.values():
			each_dopant.find_populations(self.beam_list)



	def rare_earth_gain(self, dz, the_beam):
		phase = np.zeros(the_beam.A.shape, dtype='complex64')

		for each_dopant in self.dopants.values():
			phase[0] += each_dopant.gain(the_beam) * dz

		phase[1] = phase[0]

		return phase

	def _NLS_rare_earth_gain(self, dz):
		for each_beam in self.beam_list:
			self._phase[each_beam] += self.rare_earth_gain(dz, each_beam)


	def _NLS_freq(self, dz):
		self._NLS_rare_earth_gain(dz)
		super()._NLS_freq(dz)





class RareEarthAmplificationSaturation(RareEarthAmplification):
	def __call__(self, *signals, pumps=[], **kwargs):
		self.signal_list = beam.BeamList(signals).copy(Nsaves=self.Nsaves, **kwargs)
		self.pump_list = beam.BeamList(pumps).copy(Nsaves=self.Nsaves, **kwargs)

		self.beam_list = self.signal_list + self.pump_list

		return self._NLS()


	def find_populations(self):
		for each_dopant in self.dopants.values():
			each_dopant.find_populations(pumps=self.pump_list, signals=self.signal_list)


	def gain_saturation(self, dz, the_beam):
		phase = np.zeros(the_beam.A.shape, dtype='complex64')

		for each_dopant in self.dopants.values():
			phase[0] += each_dopant.gain_saturation(the_beam) * dz

		phase[1] = phase[0]

		return phase


	def _NLS_rare_earth_gain_saturation(self, dz):
		for each_beam in self.beam_list:
			self._phase[each_beam] += self.gain_saturation(dz, each_beam)


	def _NLS_time(self, dz):
		self._NLS_rare_earth_gain_saturation(dz)

		super()._NLS_time(dz)



class RareEarthAmplificationSaturationCounterPropagatingPump(RareEarthAmplificationSaturation):
	def rare_earth_gain(self, dz, the_beam):
		phase = super().rare_earth_gain(dz, the_beam)

		if the_beam in self.pump_list:
			return -phase
		else:
			return phase

	def gain_saturation(self, dz, the_beam):
		phase = super().gain_saturation(dz, the_beam)

		if the_beam in self.pump_list:
			return -phase
		else:
			return phase




class RareEarthRamanAmplification(RareEarthAmplification, raman.RamanAmplification):
	pass


class RareEarthRamanAmplificationSaturation(RareEarthAmplificationSaturation, RareEarthRamanAmplification):
	pass

class RareEarthRamanAmplificationSaturationCounterPropagatingPump(RareEarthAmplificationSaturationCounterPropagatingPump,\
		RareEarthRamanAmplification):
	pass

class RareEarthAmplificationSaturationGainTrack(evolution.GainTrackEvolution, RareEarthAmplificationSaturation):
	pass


class RareEarthRamanAmplificationGainTrack(evolution.GainTrackEvolution, RareEarthRamanAmplification):
	pass


class RareEarthRamanAmplificationGainTrack2(evolution.GainTrackEvolution2, RareEarthRamanAmplification):
	def _NLS_init(self):
		self.energy_gain_trackers = ['Raman', 'Yb']
		super()._NLS_init()

	def _NLS_rare_earth_gain(self, dz):
		super()._NLS_rare_earth_gain(dz)
		self._NLS_update_tracker('Yb', 'freq')

	def _NLS_raman(self, dz):
		super()._NLS_raman(dz)
		self._NLS_update_tracker('Raman', 'time')



class RareEarthRamanAmplificationSaturationGainTrack(evolution.GainTrackEvolution2, RareEarthRamanAmplificationSaturation):
	def _NLS_init(self):
		self.energy_gain_trackers = ['Raman', 'Yb', 'Yb_sat']
		super()._NLS_init()

	def _NLS_rare_earth_gain(self, dz):
		super()._NLS_rare_earth_gain(dz)
		self._NLS_update_tracker('Yb', 'freq')

	def _NLS_rare_earth_gain_saturation(self, dz):
		super()._NLS_rare_earth_gain_saturation(dz)
		self._NLS_update_tracker('Yb_sat', 'time')

	def _NLS_raman(self, dz):
		super()._NLS_raman(dz)
		self._NLS_update_tracker('Raman', 'time')





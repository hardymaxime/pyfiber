# -*- coding: utf-8 -*-
#defining NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

from scipy.special import wofz

import numpy as np
cimport numpy as np


def wmean(np.ndarray x, np.ndarray y, axis=None):
	return np.sum(y*x,axis)/np.sum(y,axis)
	
def wstd(x, y, axis=None):
	try:
		return np.sqrt(np.sum(y*(x-wmean(x,y,axis))**2,axis)/np.sum(y,axis))
	except:
		return np.sqrt(np.sum(y*(x[None,:] - wmean(x,y,axis)[:,None])**2.,axis)/np.sum(y,axis))


def _ifwhm(y, x):
	return fwhm(x,y)


def fwhm(x, y, axis=None, max_value=None): 
	cdef int m, hi, lo, imax
	cdef float half
	if x.shape == y.shape:
		imax = len(y)
		m = np.argmax(y)
		hi, lo = m+1, m-1
		half = 0.5 * y[m] if max_value is None else 0.5 * max_value
		#half = 0.5 * y[m]
		
		while lo > 0 and y[lo] > half:
			lo -= 1
		
		while hi < imax-1 and y[hi] > half:
			hi += 1

		return np.abs(x[hi] - x[lo])

	else:
		return np.apply_along_axis(_ifwhm,axis,y,x)


def derivative(np.ndarray arr):
	cdef int i
	cdef float h
	result = np.empty(len(arr))

	for i in range(2,len(arr)-2):
		result[i] = (8.*arr[i+1] - 8.*arr[i-1] - arr[i+2] + arr[i-2])/12.

	result[1] = (arr[2] - arr[0])/2
	result[-2] = (arr[-1] - arr[-3])/2
	result[0] = result[1]
	result[-1] = result[-2]

	return result
			

def matrix_multiply(a, b, dtype='complex64'):
	cdef int i, length
	assert(a.shape == b.shape)

	res = np.empty(a.shape, dtype=dtype)

	if len(a.shape) == 2:
		res = np.dot(a, b)
	else:
		length = len(a)

		for i in range(length):
			res[i] = np.dot(a[i], b[i])
	
	return res



def first_order_linear_diff_eq(x, f, g, y0=0.):
	''' Finds 'y' in the differential equation d y(x)/dx + y(x) f(x) = g(x)'''
	assert(x.shape == f.shape and f.shape == g.shape)

	cdef int i
	cdef float dx = x[1] - x[0]
	cdef np.ndarray y = np.empty(f.shape, dtype=f.dtype)
	cdef np.ndarray the_range = np.arange(1, len(x))

	cdef np.ndarray g_dx = g * dx
	cdef np.ndarray f_dx = f * dx

	y[0] = (y0 + g_dx[0]) * np.exp(-f_dx[0])

	for i in the_range:
		y[i] = (y[i-1] + g_dx[i]) * np.exp(-f_dx[i])

	return y



# -*- coding: utf-8 -*-
import numpy as np
from scipy.constants import c, hbar, pi
from scipy.optimize import curve_fit

from . import tools, mode_LP



########################## RELATIVE BEAM DATA FUNCTIONS #####################################

def delay(first, second, *args, **kwargs):
	return tools.wmean(second.T,second.P(*args,**kwargs)) - tools.wmean(first.T,first.P(*args,**kwargs))


def instantaneous_frequency_difference(first, second, T=0., **kwargs):
	i = np.argmin(np.abs(first.T - T))
	return (first.w0 + first.chirp(**kwargs)[i] - second.w0 - second.chirp(**kwargs)[i]) / (2*pi)


def soliton_order(the_beam, the_fiber, **kwargs):
	return np.sqrt(np.abs(the_fiber.gamma(the_beam) * the_beam.Pmax(**kwargs) * the_beam.DT(FWHM=True,**kwargs)**2 /\
			the_fiber.beta.transpose(the_beam.carrier_l0(**kwargs))[2]))


########################## BEAM GENERATION FUNCTIONS ########################################

def _polarization(A1D, pol='x'):
	#A1D = [A1D] if not hasattr(A1D, '__len__') else A1D
	#null = [0.]*len(A1D)

	null = [0.]*len(A1D) if hasattr(A1D, '__len__') else 0.

	if pol is 'x':
		A = np.array([A1D, A1D]) / np.sqrt(2)
	elif pol is 'y':
		A = 1j * np.array([A1D,-A1D]) / np.sqrt(2)
	elif pol is '+' or pol == 0:
		A = np.array([A1D,null])
	elif pol is '-' or pol == 1:
		A = np.array([null,A1D])
	else:
		raise AttributeError("Polarization must be either 'x','y','+','-'")

	return A



def gen_polarized_cw_beam(l0, P, pol='x', **kwargs):
	A  = _polarization(np.sqrt(P), pol)
	return Beam(l0, A, **kwargs)



def gen_chirp_pulse(l0, Npts, T, energy, DT_c, DT_e=None, sign=1., pol='x', shape='gauss', FWHM=True, **kwargs):
	assert abs(sign) == 1.

	if isinstance(T,float):
		dT = 2 * T / (Npts - 1)
		T = np.arange(-Npts/2, Npts/2) * dT
	elif len(T) == 2:
		dT = (T[1] - T[0]) / (Npts - 1)
		T = np.arange(0, Npts) * dT + T[0]
	else:
		pass


	if DT_e is None:
		C = 0
		DT_e = DT_c
	else:
		C = sign * np.sqrt( (DT_e/DT_c)**2. - 1.)

	if shape == 'sech' and FWHM:
		A = np.exp(-2j*np.log(2)*C*(T/DT_e)**2) / np.cosh(2*np.arccosh(np.sqrt(2))*T/DT_e)
	elif shape == 'sech' and not FWHM:
		A = np.exp(-1j*C*(T/DT_e)**2) / np.cosh(pi*T/(DT_e*np.sqrt(3)))
	elif shape == 'gauss' and FWHM:
		A = np.exp(-2*(1+1j*C)*np.log(2) * (T/DT_e)**2)
	elif shape == 'gauss' and not FWHM:
		A = np.exp(-(1+1j*C) * (T/DT_e)**2)
	elif np.isreal(shape) and FWHM:
		A = np.exp(-2j*np.log(2)*C*(T/DT_e)**2 - 2**(shape-1)*np.log(2)*np.abs(T/DT_e)**shape)
	elif np.isreal(shape) and not FWHM:
		A = np.exp(-1j*C*(T/DT_e)**2 - np.abs(T/DT_e)**shape)
	else:
		raise AttributeError("'shape' attribute should be 'gauss', 'sech' or a supergaussian order")
	
		
	#A = np.exp(-1j*sign*C*(T/DT_e)**2-np.abs(T/DT_e)**order)
	A = _polarization(A,pol)

	p = PulseTrain(l0=l0, T=T, A=A, **kwargs)
	p.set_energy(energy)

	return p



def read_pulse_train(filestr, l0, **kwargs):
	solfinx = np.loadtxt(filestr + '_solfinx.txt')
	solfiny = np.loadtxt(filestr + '_solfiny.txt')

	Ax = solfinx[1]*np.exp(1j*solfinx[2])
	Ay = solfiny[1]*np.exp(1j*solfiny[2])

	A = _polarization(Ax, pol='x') + _polarization(Ay, pol='y')

	return PulseTrain(l0=l0, T=solfinx[0], A=A, **kwargs)



########################### NOISE GENERATION FUNCTIONS #######################################

def gen_noise(l0, Npts, T, P, pulsed=True, pol='x', **kwargs):
	if isinstance(T,float):
		dT = 2 * T / (Npts - 1)
		T = np.arange(-Npts/2, Npts/2) * dT
	elif len(T) == 2:
		dT = (T[1] - T[0]) / (Npts - 1)
		T = np.arange(0, Npts) * dT + T[0]
	else:
		pass

	if pulsed is True:
		noise = PulseTrain(l0=l0, T=T, A=np.empty((2,Npts)), **kwargs)
	else:
		noise = AmplifiedSpontaneousEmission(l0=l0, T=T, A=np.empty((2,Npts)), **kwargs)
	
	phase = np.random.random(len(T)) * 2. * pi
	At = np.exp(1j*phase)

	normalization = np.sqrt(P / (np.sum(abs(At)**2.) * noise.dw * noise.rate))

	At *= normalization

	At = _polarization(At,pol)

	noise.set_At(At)

	return noise

	


#def add_flat_noise(pulse, P, pol='x', **kwargs):
#	p = pulse.copy(**kwargs)
#	phase = np.random.random(pulse.Npts) * 2. * pi
#	At = np.exp(1j*phase)
#
#	#normalization = np.sqrt(P/(np.sum(abs(At)**2.)*p.dw*p.rate))
#	normalization = np.sqrt(P*(p.T[-1]-p.T[0])/(np.sum(abs(At)**2.)*p.dw)) # En moyenne, puissance égale à P
#	At *= normalization
#
#	At, pol = _polarization(At,pol)
#
#	p.At += At
#	p.update_A()o
#
#	return p
#




########################## BEAM MANIPULATION FUNCTIONS ######################################

def combine_pulse_trains(pulse1, pulse2, delay=0., l0=None, **kwargs):
	assert(np.allclose(pulse1.T,pulse2.T))
	assert(pulse1.rate == pulse2.rate)
	
	if l0 is None:
		l0 = 2./(1./pulse1.l0 + 1./pulse2.l0)
		
	w0 = 2*pi*c/l0
	
	A = np.zeros(pulse1.A.shape, dtype='complex64')
	A[0] += pulse1.A(**kwargs)[0] * np.exp(-1j*(pulse1.w0-w0)*pulse1.T)
	A[0] += pulse2.A(**kwargs)[0] * np.exp(-1j*((pulse2.w0-w0)*pulse2.T - pulse2.w*delay))
	
	A[1] += pulse1.A(**kwargs)[1] * np.exp(-1j*(pulse1.w0-w0)*pulse1.T)
	A[1] += pulse2.A(**kwargs)[1] * np.exp(-1j*((pulse2.w0-w0)*pulse2.T - pulse2.w*delay))
	
	p = PulseTrain(l0=l0, T=pulse1.T, rate=pulse1.rate, A=A, **kwargs)
	
	return p



def interpolate_pulse(pulse, Npts, T=None, **kwargs):
	if T is None:
		T = np.linspace(pulse.T[0],pulse.T[-1],Npts)
	if isinstance(T,float):
		T = np.linspace(-T,T,Npts)
	elif len(T) == 2:
		T = np.linspace(T[0],T[1],Npts)
	else:
		pass


	P = np.zeros((2,Npts))
	phi = np.zeros((2,Npts))

	P[0] = np.interp(T, pulse.T, pulse.P(pol=0))
	P[1] = np.interp(T, pulse.T, pulse.P(pol=1))

	phi[0] = np.interp(T, pulse.T, pulse.phase(pol=0))
	phi[1] = np.interp(T, pulse.T, pulse.phase(pol=1))

	p = PulseTrain(l0=pulse.l0, T=T, A=np.sqrt(P)*np.exp(1j*phi), rate=pulse.rate, **kwargs)

	return p



def maximum_pulse_compression(pulse, T=0., **kwargs):
	p = pulse.copy(**kwargs)

	At = np.zeros((2,pulse.Npts), dtype='complex64')
	At = np.abs(pulse.At(**kwargs))

	p.set_At(At)

	return p


def center_beam_in_window(*beams, delay=0., **kwargs):
	p_list = []
	for pulse in beams:
		p = pulse.copy(**kwargs)

		the_delay = delay - p.pulse_delay(**kwargs)
		p.freq_gain(np.exp(1j * p.w * the_delay))

		p_list.append(p)

	return p_list
	

########################## BEAM LISTS #######################################################

class BeamList(list):
	def copy(self, **kwargs):
		cp = []
		for each_beam in self:
			cp.append(each_beam.copy(**kwargs))

		return BeamList(cp)

	def Pmean(self, *args, **kwargs):
		res = 0.
		for each_beam in self:
			res += each_beam.Pmean(*args, **kwargs)

		return res

	

def make_beam_list(the_beam):
	if isinstance(the_beam,list) or isinstance(the_beam,tuple):
		return BeamList(the_beam)
	elif isinstance(the_beam,Beam):
		return BeamList([the_beam])
	else:
		raise TypeError("'the_beam' argument must be either a list of 'Beam' or a 'Beam' instance")



########################## BEAM CLASS #######################################################

class ComplexBeamAmplitude(tools.Saved):
	def _polarization(self, pol=None):
		if pol == 'x':
			P = 0.5 * np.array([[1,1],[1,1]])
		elif pol == 'y':
			P = 0.5 * np.array([[1,-1],[-1,1]])
		elif pol == '+' or pol == 0:
			P = np.array([[1,0],[0,0]])
		elif pol == '-' or pol == 1:
			P = np.array([[0,0],[0,1]])
		else:
			P = np.array([[1,0],[0,1]])

		return P


	def __call__(self, i=None, val=None, pol=None, **kwargs):
		A = super().__call__(i, val)
		
		if pol is not None:
			# La version "polarisée" se trouve avec un produit de matrices.
			# Il se fait avec "tensordot" car il peut être compatible avec les classes "Beam" et "PulseTrain"
			A = np.tensordot(self._polarization(pol), A, axes=([0],[0-len(self.shape)]))

		elif len(A.shape) > len(self.shape):
			A = A.swapaxes(0,1)

		if len(self.shape) > 1:
			A = np.fft.fftshift(A, axes=-1)

		return A

	
	def set(self, val, index=None):
		if not isinstance(val, tools.Saved) and len(self.shape) > 1:
			val = np.fft.fftshift(val, axes=-1)

		super().set(val, index)

	
	def __imul__(self, other):
		if isinstance(other, np.ndarray) and len(self.shape) > 1:
			other = np.fft.fftshift(other, axes=-1)

		return super().__imul__(other)

	
	def __getitem__(self, i):
		if len(self.shape) > 1:
			return np.fft.fftshift(super().__getitem__(i), axes=-1)
		else:
			return super().__getitem__(i)
	


class Beam(tools.Copyable2):
	def __init__(self, l0, A=None, z=0., Nsaves=1, mode=(0,1), **kwargs):
		super().__init__(**kwargs)

		# Save
		self.Nsaves = Nsaves

		# Carrier definition
		self.l0 = l0
		self.w0 = 2*pi*c/l0
		self.f0 = c/l0

		# Definition of frequency and wavelength
		self.w = 0
		self.l = self.l0
		self.f = self.f0
		
		if mode == 'multimode' or isinstance(mode, mode_LP.Multimode):
			self.mode = mode_LP.Multimode()
		elif isinstance(mode, tuple):
			self.mode = mode_LP.ModeLP(*mode)
		elif isinstance(mode, mode_LP.ModeLP):
			self.mode = mode_LP.ModeLP(mode.l, mode.m)
	
		# Amplitude array definition
		self.A = ComplexBeamAmplitude(A, self.Nsaves, dtype='complex64')
		self.At = ComplexBeamAmplitude(A, self.Nsaves, dtype='complex64')

		# Position array definition
		self.z = tools.Saved(z,self.Nsaves)

		# Unit dimensions
		self.unit = tools.UnitDict()
		self.unit.adjust({'l0':self.l0})

		self.copy_args += ['l0', 'A', 'z', 'Nsaves', 'mode']



	def copy(self, **kwargs):
		the_copy = super().copy(**kwargs)

		if 'l0' in kwargs:
			w1 = 2*pi*c/kwargs['l0']
			the_copy.A *= np.exp(1j * self.T * (w1 - self.w0))
			the_copy.update_At()


		return the_copy
		
	

	def set_Pmean(self, Pmean):
		self.A *= np.sqrt(Pmean/self.Pmean())
		self.update_At()



	def info(self, unit={}, **kwargs):
		i = self._index(**kwargs)
		z = self.z(i)
		Pmean = self.Pmean(**kwargs)

		self.unit.adjust({'z':z, 'Pmean':Pmean})
		self.unit.update(unit)

		string = object.__repr__(self) + '\n\n'
		
		string += 'z = {:0.4g} {}m\n'.format(z/self.unit['z'], self.unit.prefix('z'))
		string += 'l0 = {:0.4g} {}m\n\n'.format(self.l0/self.unit['l0'], self.unit.prefix('l0'))
		string += '<P> = {:0.4g} {}W\n'.format(Pmean/self.unit['Pmean'], self.unit.prefix('Pmean'))

		return string

		
	def __str__(self):
		return self.info()


	def save(self):
		self.A.save()
		self.At.save()
		self.z.save()


	# Analysis methods

	def P(self, *args, **kwargs):
		A = self.A(self.index(*args, **kwargs), **kwargs)
		return np.abs(A[0])**2. + np.abs(A[1])**2.


	def Pmean(self, *args, **kwargs):
		return self.P(*args, **kwargs)


	def gain(self, *args, **kwargs):
		pol = kwargs['pol'] if 'pol' in kwargs else None
			
		return self.Pmean(*args, **kwargs)/self.Pmean(0, pol=pol)

	
	def gain_dB(self, *args, **kwargs):
		return tools.ratio_to_dB(self.gain(*args, **kwargs))	


	def stokes(self, *args, **kwargs):
		A = self.A(self.index(*args, **kwargs), **kwargs)

		# Calcul des coefficients de Stokes en polarisation circulaire
		I = np.abs(A[0])**2. + np.abs(A[1])**2.
		Q = 2. * np.real(A[0] * np.conj(A[1]))
		U = - 2. * np.imag(A[0] * np.conj(A[1]))
		V = np.abs(A[0])**2. - np.abs(A[1])**2.

		return np.array([I,Q,U,V])



	def set_A(self, A):
		self.A.set(A)
		self.update_At()

	
	def set_At(self, At):
		self.At.set(At)
		self.update_A()
	

	# Update methods
	def update_At(self):
		self.At.set(self.A())

	def update_A(self):
		self.A.set(self.At())

	
	def freq_gain(self, gain):
		self.At *= gain
		self.update_A()

	
	def time_gain(self, gain):
		self.A *= gain
		self.update_At()
	




########################## PULSE TRAIN SUBCLASS #############################################

class PulseTrain(Beam):
	"""Class defining objects representing pulses which propagate in optical fibers

	The 'Pulse' object has some properties:
	T: \tThe time domain vector
	l: \tThe wavelength domain
	f,w: \tThe frequency domain (in Hz or rad/s)
	A: \tThe slowly-varying amplitude of the electrical field
	At: \tThe Fourier transform of A
	dT: \tThe time interval between two sampling points
	dw: \tThe frequency interval between two sampling points

	The 'Pulse' object has many analysis methods:
	P: \tReturns the instantaneous power (in W)
	S: \tReturns the pulse spectrum
	chirp: \tReturns the instantaneous frequency in the pulse
	autocorr: \tReturns the autocorrelation trace of the pulse with itself
	DT: \tReturns the pulse duration (in s)
	Df,Dw,Dl: \tReturns the pulse spectral width (in Hz, rad/s or in m)
	Pmax: \tReturns the peak power of the pulse
	energy: \tReturns the pulse energy
	gain: \tReturns the energy gain
	C: \tReturns the linear chirp coefficient at T=0
	
	The 'Pulse' object presents two graphical t#ools
	plot: \tShows the instantaneous power, the spectrum and chirp for given travelled distance 'z'
	hist: \tShows the pulse history, i.e. the variation of many analysis tools as a function of travelled distance 'z'

	Moreover, the pulse object can be modified through the assignement of the properties 'A' and 'At'.
	Also, the methods 'update_A' and 'update_At' can be used to recalculate 'A' and 'At' through the Fourier transform.

	The properties of a pulse can be copied for a given travelled distance 'z' by using the __call__ method of the pulse object:
	e.g. \tp = Pulse(T,A1,A2)
	\tq = p(z=0.)


	"""
	def __init__(self, T, rate=100e6, fftw_flags=['FFTW_MEASURE'], **kwargs):
		"""Constructor arguments:

		T: \tA numpy array containing the time domain of the pulse
		A1: \tThe numpy array containing the amplitude and phase of the first polarization ('x' by default)
		A2: \tThe numpy array containing the amplitude and phase of the second polarization ('y' by default)
		Nsaves: \tThe number of copy of the pulse to keep in memory when propagating in a fiber
		circ: \tA flag. If 'False' (by default), A1 and A2 represent the 'x' and 'y' polarizations
		\tIf 'True', A1 and A2 represent the '+' and '-' circular polarizations
		l0: \tThe carrier wavelength (1550 nm by default)
		z0: \tThe initial distance travelled in the fiber (0 m by default)
		rate: \tThe repetition rate of the pulses (100 MHz by default)
		fftw: \tA list of flags to give to the FFTW plans (['measure'] by default)

		"""
		# Beam initialization
		super().__init__(**kwargs)

		# Nombre de points de la grille
		self.Npts = len(T)

		# Définition des transformées de Fourier et mesures des plans
		self.fftw_flags = fftw_flags
		self.FFTW = tools.FFTW(self.At.raw(), self.A.raw(), fftw_flags=self.fftw_flags, axes=(-1,))

		# Définition des vecteurs T et w, ainsi les pas dT et dw
		self.T = T
		self.dT = (np.amax(self.T)-np.amin(self.T)) / (self.Npts - 1)

		self.dw = 2 * pi / (self.Npts * self.dT)
		self.w = np.arange(-self.Npts/2, self.Npts/2) * self.dw

		# Definition of frequencies and wavelenghts vectors
		self.f = (self.w + self.w0)/(2.*pi)
		self.l = 2.*pi*c/(self.w + self.w0)

		# Définition des tableaux A et At
		self.A.set(kwargs['A'])
		self.update_At()

		# Taux de répétition
		self.rate = rate

		self.unit.adjust({'rate':self.rate})

		self.copy_args += ['T', 'rate', 'fftw_flags']


	def set_A(self, A):
		self.A.set(A)
		self.update_At()


	def set_At(self, At):
		self.At.set(At)
		self.update_A()


	def update_At(self):
		self.FFTW.ifftw()
		self.At *= self.Npts * self.dT / np.sqrt(2 * pi)


	def update_A(self):
		self.FFTW.fftw()
		self.A *= np.sqrt(2 * pi) / (self.dT * self.Npts)

	
	def save(self):
		super().save()

		# Changement de tableaux pour les transformées de Fourier
		self.FFTW.update_arrays(self.At.raw(), self.A.raw())
		

	def set_energy(self, energy):
		self.A *= np.sqrt(energy/self.energy())
		self.update_At()


	def time_gain(self, gain):
		self.A *= gain

		self.update_At()


	def freq_gain(self, gain):
		self.At *= gain

		self.update_A()


	# Analysis methods
	def P(self, *args, **kwargs):
		A = self.A(self.index(*args, **kwargs), **kwargs)
		return np.abs(A[0])**2 + np.abs(A[1])**2


	def S(self, *args, **kwargs):
		At = self.At(self.index(*args, **kwargs), **kwargs)
		return np.abs(At[0])**2 + np.abs(At[1])**2


	def autocorr(self, *args, **kwargs):
		AC = np.fft.ifftshift(np.fft.ifft(np.abs(np.fft.fft(self.P(*args, **kwargs)))**2.),axes=-1)
		return np.abs(AC/np.amax(AC))


	def chirp(self, *args, fit=None, **kwargs):
		''' Find the chirp in the '0' polarisation. If the chirp in the '0' is null, find the chirp in the '1' polarisation '''
		phase = self.phase(*args, **kwargs)

		res = -tools.derivative(phase)/self.dT

		if fit is not None:
			lim = tools.limit(self.T, -2*self.DT(), 2*self.DT())
			popt, pcov = curve_fit(tools.polynom, self.T[lim], res[lim], fit)
			return tools.polynom(self.T, *popt)
		else:
			return res


	def phase(self, *args, **kwargs):
		A = self.A(self.index(*args, **kwargs), **kwargs)[0]

		if np.allclose(A, 0):
			A = self.A(self.index(*args, **kwargs), **kwargs)[1]
			
		return np.unwrap(np.angle(A))


	def spectral_phase(self, *args, **kwargs):
		At = self.At(self.index(*args, **kwargs), **kwargs)[0]

		if np.allclose(At, 0):
			A = self.At(self.index(*args, **kwargs), **kwargs)[1]
			
		return np.unwrap(np.angle(At))


	def beta(self, initial_beta, get_pcov=False, spectral_width_coeff=1):
		w0 = self.carrier_w0()
		lim = tools.limit(self.w + self.w0, w0-spectral_width_coeff*self.Dw(), w0+spectral_width_coeff*self.Dw())

		popt, pcov = curve_fit(tools.polynom, self.w[lim], self.spectral_phase()[lim], initial_beta)
		
		if get_pcov:
			return tools.Beta(self.l0,popt), pcov
		else:
			return tools.Beta(self.l0,popt)
	

	def DT(self, *args, FWHM=False, autocorr=False, soliton=False, **kwargs):
		if autocorr:
			y = self.autocorr(*args, **kwargs)
		else:
			y = self.P(*args, **kwargs)

		if FWHM:
			return tools.fwhm(self.T,y,axis=-1)
		elif soliton:
			return 2.*tools.wstd(self.T,y,axis=-1) * np.sqrt(3) / pi
		else:
			return 2.*tools.wstd(self.T,y,axis=-1)


	def Dl(self, *args, FWHM=False, **kwargs):
		if FWHM:
			return tools.fwhm(self.l,self.S(*args, **kwargs),axis=-1)
		else:
			return 2.*tools.wstd(self.l,self.S(*args, **kwargs),axis=-1)
	

	def Dw(self, *args, FWHM=False, **kwargs):
		if FWHM:
			return tools.fwhm(self.w,self.S(*args, **kwargs),axis=-1)
		else:
			return 2.*tools.wstd(self.w,self.S(*args, **kwargs),axis=-1)

		
	def Df(self, *args, **kwargs):
		return self.Dw(*args,**kwargs)/(2*pi)


	def carrier_l0(self, *args, **kwargs):
		return tools.wmean(self.l, self.S(*args, **kwargs), axis=-1)


	def carrier_w0(self, *args, **kwargs):
		return tools.wmean(self.w + self.w0, self.S(*args, **kwargs),axis=-1)

	
	def l_T(self, *args, **kwargs):
		return 2 * pi * c / (self.w0 + self.chirp(*args, **kwargs))


	def carrier_f0(self, *args, **kwargs):
		return self.carrier_w0(*args, **kwargs) / (2*pi)	

	
	def pulse_delay(self, *args, maximum=False, **kwargs):
		return self.T[np.argmax(self.P(*args, **kwargs))] if maximum else tools.wmean(self.T,self.P(*args, **kwargs), axis=-1)


	def Pmax(self, *args, **kwargs):
		return np.amax(self.P(*args, **kwargs), axis=-1)
			
	def energy(self, *args, **kwargs):
		return np.sum(self.P(*args, **kwargs),axis=-1) * self.dT

	def Pmean(self, *args, **kwargs):
		return self.energy(*args, **kwargs) * self.rate
		
	def C(self, *args, **kwargs):
		m = np.argmax(self.P(*args, **kwargs), axis=-1)
		try:
			return 0.5 * self.DT(*args, **kwargs)**2 * tools.derivative(self.chirp(*args, **kwargs))[...,m][...,-1]/self.dT
		except:
			return 0.5 * self.DT(*args, **kwargs)**2 * tools.derivative(self.chirp(*args, **kwargs))[...,m]/self.dT
	


	def info(self, unit={}, **kwargs):
		i = self.index(**kwargs)

		z = self.z(i)
		carrier_l0 = self.carrier_l0(**kwargs)
		T = self.pulse_delay(**kwargs)
		Pmean = self.Pmean(**kwargs)
		energy = self.energy(**kwargs)
		Pmax = self.Pmax(**kwargs)
		DT, DT_FWHM = self.DT(**kwargs), self.DT(FWHM=True, **kwargs)
		Dl, Dl_FWHM = self.Dl(**kwargs), self.Dl(FWHM=True, **kwargs)
		Df, Df_FWHM = self.Df(**kwargs), self.Df(FWHM=True, **kwargs)

		self.unit.adjust({'z':z, 'Pmean':Pmean, 'carrier_l0':carrier_l0, 'T':T, 'energy':energy, 'Pmax':Pmax,\
				'DT':DT, 'DT_FWHM':DT_FWHM, 'Dl':Dl, 'Dl_FWHM':Dl_FWHM, 'Df':Df, 'Df_FWHM':Df_FWHM})
		self.unit.update(unit)

		string = object.__repr__(self) + '\n\n'
		
		string += 'z = {:0.4g} {}m\n'.format(z/self.unit['z'], self.unit.prefix('z'))
		string += 'l0 = {:0.4g} {}m\t'.format(self.l0/self.unit['l0'], self.unit.prefix('l0'))
		string += '(estimated) = {:0.4g} {}m\n'.format(carrier_l0/self.unit['carrier_l0'], self.unit.prefix('carrier_l0'))
		string += 'T = {:0.4g} {}s\n\n'.format(T/self.unit['T'], self.unit.prefix('T'))

		string += '<P> = {:0.4g} {}W\n'.format(Pmean/self.unit['Pmean'], self.unit.prefix('Pmean'))
		string += 'E = {:0.4g} {}J\n'.format(energy/self.unit['energy'], self.unit.prefix('energy'))
		string += 'Pmax = {:0.4g} {}W\n'.format(Pmax/self.unit['Pmax'], self.unit.prefix('Pmax'))
		string += 'Rate = {:0.4g} {}Hz\n\n'.format(self.rate/self.unit['rate'], self.unit.prefix('rate'))

		string += 'DT = {:0.4g} {}s\t'.format(DT/self.unit['DT'], self.unit.prefix('DT'))
		string += '(FWHM) = {:0.4g} {}s\n'.format(DT_FWHM/self.unit['DT_FWHM'], self.unit.prefix('DT_FWHM'))
		string += 'Dl = {:0.4g} {}m\t'.format(Dl/self.unit['Dl'], self.unit.prefix('Dl'))
		string += '(FWHM) = {:0.4g} {}m\n'.format(Dl_FWHM/self.unit['Dl_FWHM'], self.unit.prefix('Dl_FWHM'))
		string += 'Df = {:0.4g} {}Hz\t'.format(Df/self.unit['Df'], self.unit.prefix('Df'))
		string += '(FWHM) = {:0.4g} {}Hz\n'.format(Df_FWHM/self.unit['Df_FWHM'], self.unit.prefix('Df_FWHM'))

		return string


	def __getstate__(self):
		# Don't pickle FFTW objects
		state = self.__dict__.copy()
		del state['FFTW']
		return state


	def __setstate__(self, state):
		# Recreate FFTW objects after unpickling
		self.__dict__ = state
		self.FFTW = tools.FFTW(self.At.raw(),self.A.raw(), fftw_flags=self.fftw_flags, axes=(-1,))


class AmplifiedSpontaneousEmission(PulseTrain):
	def __init__(self, T, **kwargs):
		rate = 1 / (T[-1] - T[0])

		super().__init__(T=T, rate=rate, **kwargs)

		self.copy_args.remove('rate')


	



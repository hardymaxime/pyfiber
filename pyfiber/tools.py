# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import pyximport
pyximport.install()

import numpy as np
import numpy.core.umath_tests
import pyfftw
import pickle, pkg_resources
from scipy.special import wofz
from scipy.constants import pi, c
from scipy.misc import factorial
from os import system
from .cython_tools import fwhm, matrix_multiply, first_order_linear_diff_eq
import matplotlib as mpl

########################## MATH FUNCTIONS ###################################################

def lorentzian(x, x0=0, gamma=1):
	return gamma**2 / (pi * (x-x0)**2. + gamma**2.)


def complex_lorentzian(x, x0=0, gamma=1):
	return 1. / (1. + 1j*np.sqrt(pi) * (x-x0)/gamma)


def gaussian(x, x0=0, sigma=1):
	return np.exp(-0.5 * ((x-x0)/sigma)**2.)


def complex_gaussian(x, x0=0, sigma=1):
	return wofz(-(x-x0)/(sigma*np.sqrt(2)))
	#return np.exp(-0.5 * ((x-x0)/sigma)**2.) * (1. - 1j*erfi((x-x0)/(sigma * np.sqrt(2))) )


def voigt(x, x0, sigma, gamma):
	'''
	Nomalized values of the complex Voigt profile
	Represents a convolution of a Gaussian of variance sigma^2 and a lorentzian of width gamma
	'''
	z = (x - x0 + 1j * gamma)/(sigma * np.sqrt(2))
	return wofz(z) / (sigma * np.sqrt(2*np.pi))


def wmean(x, y, axis=None):
	return np.sum(y*x,axis)/np.sum(y,axis)
	
def wstd(x, y, axis=None):
	try:
		return np.sqrt(np.sum(y*(x-wmean(x,y,axis))**2,axis)/np.sum(y,axis))
	except:
		return np.sqrt(np.sum(y*(x[None,:] - wmean(x,y,axis)[:,None])**2.,axis)/np.sum(y,axis))


def derivative(arr, axis=-1):
	if axis is None:
		return np.gradient(arr)
	else:
		return np.apply_along_axis(np.gradient,axis,arr)


def integral(y, dx):
	return np.sum(y * dx)

def integrate(x, y, x0=None, x1=None):
	i0 = np.argmin(np.abs(x-x0)) if x0 is not None else None
	i1 = np.argmin(np.abs(x-x1)) if x1 is not None else None
	dx = x[1] - x[0] if len(x) > 1 else 0

	return np.sum(y[slice(i0,i1)]) * dx


def limit(tab, lo=None, hi=None):
	if lo is None:
		lo = np.amin(tab)-np.abs(np.amin(tab))
	if hi is None:
		hi = np.amax(tab)+np.abs(np.amax(tab))

	return (tab >= lo)*(tab <= hi)


def dB_to_ratio(dB):
	return 10.**(dB/10.)


def ratio_to_dB(ratio):
	return 10. * np.log10(ratio)


def P_to_dB(P, P_ref=1e-3):
	return 10. * np.log10(P/P_ref)


def dB_to_P(dB, P_ref=1e-3):
	return P_ref * 10.**(dB/10.)


def V_number(wavelength, cutoff):
	return cutoff * 2.405 / wavelength


def expi(tab):
	''' Faster exponential function for imaginary only argument'''
	res = np.zeros(len(tab), dtype='complex128')
	res.real += np.cos(tab)
	res.imag += np.sqrt(1. - res.real*res.real)
	res.imag *= np.sign(np.mod(tab + np.pi,2*np.pi) - np.pi)
	return res


def expc(tab):
	''' Faster exponential function for complex argument. A little slower thant expi.'''
	res = np.zeros(len(tab), dtype='complex128')
	res.real += np.cos(tab.imag)
	res.imag += np.sqrt(1. - res.real*res.real)
	res.imag *= np.sign(np.mod(tab.imag + np.pi,2*np.pi) - np.pi)
	res *= np.exp(tab.real)
	return res



def Fourier_transform(t, A, definition='Agrawal'):
	Npts = len(t)
	dt = (np.amax(t) - np.amin(t))/Npts
	dnu =  1 / (np.amax(t) - np.amin(t))
	nu = np.linspace(-0.5/dt, 0.5/dt - dnu, Npts)*(Npts-1)/Npts

	if definition == 'Agrawal':
		At = Npts * dt * np.fft.ifftshift(np.fft.ifft(A)) / np.sqrt(2*pi)
	else:
		At = Npts * dt * np.fft.fftshift(np.fft.fft(A)) / np.sqrt(2*pi)

	return nu, At


def inverse_Fourier_transform(nu, At, definition='Agrawal'):
	Npts = len(nu)
	dnu = (np.amax(nu) - np.amin(nu))/Npts
	dt = 1 / (np.amax(nu) - np.amin(nu))
	t = np.linspace(-0.5/dnu, 0.5/dnu - dt, Npts)*(Npts-1)/Npts

	if definition == 'Agrawal':
		A = np.sqrt(2*pi) * np.fft.fft(np.fft.fftshift(At)) / (Npts * dt)
	else:
		A = np.sqrt(2*pi) * np.fft.ifft(np.fft.ifftshift(At)) / (Npts * dt)
	
	return t, A



def polynom(x, *params):
	res = 0.
	for i, p_i in enumerate(params):
		res += p_i * x**i / factorial(i)
	
	return res


########################## FILE FUNCTIONS ###################################################

def pdfcrop(fichier1, fichier2=None):
	if fichier2 is None:
		fichier2 = fichier1
	system('pdfcrop {} {}'.format(fichier1,fichier2))


def pngcrop(fichier1, fichier2=None):
	if fichier2 is None:
		fichier2 = fichier1
	system('convert -trim {} {}'.format(fichier1,fichier2))


def save_object(obj, fichier, mode='wb'):
	with open(fichier, mode) as f:
		p = pickle.Pickler(f)
		p.dump(obj)


def load_object(fichier, mode='rb'):
	with open(fichier, mode) as f:
		p = pickle.Unpickler(f)
		return p.load()


def save_png(figure, filename, crop=True, *args, **kwargs):
	figure.savefig(filename, *args, **kwargs)
	if crop:	
		pngcrop(filename)

def save_pdf(figure, filename, crop=True, *args, **kwargs):
	figure.savefig(filename, *args, **kwargs)
	if crop:
		pdfcrop(filename)



def copy_file(file1, file2):
	system('cp {} {}'.format(file1,file2))


def open_local_file(filepath, pkgname='pyfiber'):
	return pkg_resources.resource_stream(pkgname, filepath)


########################## OTHER FUNCTIONS ##################################################


def repeat_function(func, *args):
	for i in xrange(1000):
		func(*args)


def import_wisdom_from_file(filename):
	pyfftw.import_wisdom(load_object(filename))

def export_wisdom_to_file(filename):
	save_object(pyfftw.export_wisdom(),filename)

def show_fwhm(x, y, ax=None, unit_text='', unit_multiplier=1, text_offset=0.01, color='k'):
	if ax is None:
		ax = mpl.pyplot.gca()

	m = np.argmax(y)
	h1 = np.argmin(abs(0.5*y[m] - y[0:m]))
	h2 = m + np.argmin(abs(0.5*y[m] - y[m:-1]))
	print(m, h1, h2)
	value = unit_multiplier * fwhm(x,y)

	if unit_text is not '':
		unit_text = ' ' + unit_text

	ax.annotate('', (x[h1], y[h1]), (x[h2], y[h1]), xycoords='data', arrowprops={'arrowstyle':'<->', 'shrinkA':0, 'shrinkB':0,'color':color})

	ax.text(x[(h1+h2)//2], y[h1]+text_offset*ax.get_ylim()[1], '{:0.0f}{}'.format(value, unit_text), horizontalalignment='center',color=color)

def set_figure_sizes(rules='work'):
	mpl.rcdefaults()
	if rules == 'OSA':
		#mpl.pyplot.tight_layout()
		mpl.rcParams['figure.figsize'] = (8.4/2.54, 0.75*8.4/2.54)
		mpl.rcParams['lines.linewidth'] = 0.9
		mpl.rcParams['axes.linewidth'] = 0.9
		mpl.rcParams['xtick.major.width'] = 0.75
		mpl.rcParams['xtick.major.size'] = 2
		mpl.rcParams['ytick.major.width'] = 0.75
		mpl.rcParams['ytick.major.size'] = 2
		mpl.rcParams['font.size'] = 8
		mpl.rcParams['legend.fontsize'] = 'medium'
		mpl.rcParams['legend.handlelength'] = 3
		mpl.rcParams['figure.dpi'] = 600
		#mpl.rcParams['figure.subplot.left'] = 0.13
		#mpl.rcParams['figure.subplot.right'] = 0.86
		#mpl.rcParams['figure.subplot.bottom'] = 0.14
		mpl.rcParams['axes.labelsize'] = 'large'
		mpl.rcParams['xtick.labelsize'] = 'medium'
		mpl.rcParams['ytick.labelsize'] = 'medium'
		mpl.rcParams['xtick.major.pad'] = 3
		mpl.rcParams['ytick.major.pad'] = 3
		#mpl.rcParams['text.usetex'] = True
		#mpl.rcParams['text.latex.unicode'] = True
		mpl.rcParams['font.family'] = 'serif'
		mpl.rcParams['font.serif'] = 'CMU Serif'

	elif rules == 'OSAx2':
		mpl.rcParams['figure.figsize'] = (2*8.4/2.54, 2*0.75*8.4/2.54)
		mpl.rcParams['figure.dpi'] = 300
		mpl.rcParams['font.size'] = 16
		#mpl.rcParams['font.weight'] = 700
		mpl.rcParams['text.usetex'] = True
		mpl.rcParams['text.latex.unicode'] = True
		mpl.rcParams['font.family'] = 'serif'
		mpl.rcParams['font.serif'] = 'CMU Serif'
		mpl.rcParams['axes.labelsize'] = 'large'
		mpl.rcParams['legend.fontsize'] = 'medium'
		mpl.rcParams['xtick.labelsize'] = 'small'
		mpl.rcParams['ytick.labelsize'] = 'small'
		mpl.rcParams['lines.linewidth'] = 1.75
		mpl.rcParams['axes.linewidth'] = 1.75
		mpl.rcParams['xtick.major.width'] = 1.5
		mpl.rcParams['figure.subplot.right'] = 0.88
		mpl.rcParams['ytick.major.width'] = 1.5
		

	elif rules == 'work':
		mpl.rcParams['lines.linewidth'] = 2
		mpl.rcParams['axes.linewidth'] = 1.5
		mpl.rcParams['xtick.major.width'] = 1.5
		mpl.rcParams['ytick.major.width'] = 1.5
		mpl.rcParams['font.size'] = 14
		mpl.rcParams['axes.labelsize'] = 'large'
		mpl.rcParams['text.usetex'] = True
		mpl.rcParams['text.latex.unicode'] = True
		mpl.rcParams['font.family'] = 'serif'
		mpl.rcParams['font.serif'] = 'CMU Serif'
		


########################## USEFUL CLASSES ###################################################

class FFTW(object):
	def __init__(self, tab1, tab2=None, fftw_flags=['FFTW_MEASURE'], axes=None):
		# Définition des tableaux sur lesquels s'exécutes les fft
		self.tab1 = tab1
		self.tab2 = tab2 if tab2 is not None else tab1

		# Définition des axes sur lesquels s'exécutent les fft
		self.axes = axes
		if self.axes is None:
			self.axes = [ax for ax in range(len(self.tab1.shape))]

		# Définition des transformées de Fourier et mesures des plans
		self._fftw_flags = fftw_flags
		self._fftw = pyfftw.FFTW(self.tab1, self.tab2, direction='FFTW_FORWARD', flags=self._fftw_flags, axes=self.axes)
		self._ifftw = pyfftw.FFTW(self.tab2, self.tab1, direction='FFTW_BACKWARD', flags=self._fftw_flags, axes=self.axes)


	
	def fftw(self, tab=None):
		if tab is not None:
			self.tab1 *= 0
			self.tab1 += tab

		self._fftw()

		return self.tab2


	def ifftw(self, tab=None):
		if tab is not None:
			self.tab2 *= 0
			self.tab2 += tab

		self._ifftw()

		return self.tab1


	def update_arrays(self, tab1, tab2=None):
		self.tab1 = tab1
		self.tab2 = tab2 if tab2 is not None else tab1

		self._fftw.update_arrays(self.tab1, self.tab2)
		self._ifftw.update_arrays(self.tab2, self.tab1)
		


class UnitDict(dict):
	def __init__(self, *args, **kwargs):
		dict.__init__(self, *args, **kwargs)

		self.prefix_dict = {1e-24:'y', 1e-21:'z', 1e-18:'a', 1e-15:'f', 1e-12:'p', 1e-9:'n', 1e-6:'u', 1e-3:'m',\
				1e0:'', 1e3:'k', 1e6:'M', 1e9:'G', 1e12:'T', 1e15:'P', 1e18:'E', 1e21:'Z', 1e24:'Y'}


	def __getitem__(self, key):
		if key in self.keys():
			return dict.__getitem__(self, key)
		else:
			return 1.

	
	def prefix(self, key):
		if self[key] in self.prefix_dict:
			return self.prefix_dict[self[key]]
		else:
			return '\b{} '.format(self.scientific_notation(key))

	def scientific_notation(self, key):
		return 'e{:0.0f}'.format(np.log10(self[key]))

	
	def adjust(self, value_dict):
		for key, value in value_dict.items():
			if value == 0.:
				self[key] = 1.
			else:
				self[key] = 10**(np.floor(np.log10(abs(value))/3.)*3.)
		



class Saved(object):
	def __init__(self, val=None, Nsaves=1, shape=(), **kwargs):
		if not isinstance(val, Saved):
			# Number of possible saves
			self.Nsaves = Nsaves

			# Index of the last fulfilled saves
			self._i = 0

		else:
			self.Nsaves = val.Nsaves
			self._i = val._i

		# Create the backup
		if hasattr(val,'shape'):
			shape = val.shape

		if 'dtype' not in kwargs and hasattr(val, 'dtype'):
			kwargs['dtype'] = val.dtype

		self._saved = np.zeros((self.Nsaves,)+shape, **kwargs)

		if val is not None:
			self.set(val)



	def set(self, val, index=None):
		if index is None:
			index = self._i

		if isinstance(val, Saved):
			assert(val.Nsaves == self.Nsaves)
			self._saved *= 0
			self._saved += val._saved
			self._i = val._i
		else:
			self._saved[index] = val
	

	def raw(self):
		return self._saved[self._i]


	def index(self, i=None, val=None):
		if i == 'all':
			i = slice(None,self._i+1,None)
		elif val is not None:
			i = np.argmin(abs(self._saved - val))
		elif i is None:
			i = self._i

		return i


	def save(self, **kwargs):
		if self._i < self.Nsaves-1:
			# Change of save slice
			self._i += 1

			self._saved[self._i] = self._saved[self._i-1]

			# Adding something to the save loop
			self._save_complement(**kwargs)
	
	def unsave(self):
		self._i -= 1


	def _save_complement(self, **kwargs):
		# This function can be modified by child classes to add something to the save loop
		pass



	def __repr__(self):
		return 'Saved({})'.format(self.__call__())

	def __str__(self):
		return self.__call__().__str__()

	
	def __getitem__(self, i):
		# Return an item or a slice of the last save
		try:
			return self._saved[self._i].__getitem__(i)
		except:
			raise AttributeError("'Saved' object has no attribute '__getitem__'")

	def __setitem__(self, i, val):
		try:
			self._saved[self._i].__setitem__(i,val)
		except:
			raise AttributeError("'Saved'  object does not support item assignement")


	def __call__(self, i=None, val=None, **kwargs):
		return self._saved[self.index(i,val)]


	def __getattr__(self, attr):
		try:
			return getattr(self._saved[self._i],attr)
		except:
			raise AttributeError("'Saved object has no attribute '{}'".format(attr))


	def __len__(self):
		return self._i + 1

	
	def __iadd__(self, other):
		self._saved[self._i] += other
		return self

	def __add__(self, other):
		return self._saved[self._i].__add__(other)

	def __radd__(self, other):
		return self._saved[self._i].__radd__(other)

	def __isub__(self, other):
		self._saved[self._i] -= other
		return self

	def __sub__(self, other):
		return self._saved[self._i].__sub__(other)

	def __rsub__(self, other):
		return self._saved[self._i].__rsub__(other)

	def __imul__(self, other):
		self._saved[self._i] *= other
		return self

	def __mul__(self, other):
		return self._saved[self._i].__mul__(other)

	def __rmul__(self, other):
		return self._saved[self._i].__rmul__(other)

	def __idiv__(self, other):
		self._saved[self._i] /= other
		return self

	def __div__(self, other):
		return self._saved[self._i].__div__(other)

	def __rdiv__(self, other):
		return self._saved[self._i].__rdiv__(other)
		


class Copyable(object):
	def __init__(self, **copy_args):
		self._copy_args = copy_args


	def copy(self, **copy_args):
		self._copy_args.update(copy_args)

		return type(self)(**self._copy_args)



class Copyable2(object):
	def __init__(self, **kwargs):
		self.copy_args = []
	

	def index(self, i=None, **kwargs):
		for key in (k for k in kwargs if k in self.__dict__ and isinstance(self.__dict__[k], Saved)):
			i = self.__dict__[key].index(val=kwargs[key])

		return i



	def copy(self, **kwargs):
		copy_dict = {}
		i = self.index(**kwargs)

		if 'Nsaves' not in kwargs and 'Nsaves' in self.copy_args:
			copy_dict['Nsaves'] = 1

		for key in self.copy_args:
			if key in kwargs:
				copy_dict[key] = kwargs[key]

			elif key not in copy_dict:
				if key in self.__dict__ and isinstance(self.__dict__[key], Saved):
					copy_dict[key] = self.__dict__[key](i, **kwargs)
				
				else:
					copy_dict[key] = self.__dict__[key]
		
		return type(self)(**copy_dict)



class Beta(list):
	'''Beta object must contain a list of beta_i values,
	starting with beta_0 and reaching the highest known value
	of beta_i for the wavelength l0'''
	def __init__(self, l0, *args, **kwargs):
		list.__init__(self, *args, **kwargs)

		self.l0 = l0
		self.w0 = 2*pi*c/self.l0

		self._unit = UnitDict()
		self._unit.adjust({'l0':self.l0})

	def __call__(self, the_beam):
		if hasattr(the_beam, 'Npts'):
			beta_res = np.zeros(the_beam.Npts)
		else:
			beta_res = 0.

		for i, beta_i in enumerate(self):
			beta_res += (the_beam.w + the_beam.w0 - self.w0)**i * beta_i / factorial(i)

		return beta_res

	
	def __repr__(self):
		string = '['
		for beta_i in self[0:-1]:
			string += '{:0.4g}, '.format(beta_i)
		string += '{:0.4g}]'.format(self[-1])
		string += ' at {:0.4g} {}m'.format(self.l0/self._unit['l0'], self._unit.prefix('l0'))
		return string



	def transpose(self, l0=None, end=None):
		if l0 is None:
			l0 = self.l0

		new_beta = Beta(l0,[])
		Dw = new_beta.w0 - self.w0

		for i in range(end+1 if end is not None else len(self)):
			beta_i = 0.
			for j, beta_j in enumerate(self[i:]):
				beta_i += Dw**j * beta_j / factorial(j)

			new_beta.append(beta_i)

		return new_beta

	def __neg__(self):
		new_beta = Beta(self.l0,[])
		for i in range(len(self)):
			new_beta.append(-self[i])

		return new_beta


	def __mul__(self, other):
		new_beta = Beta(self.l0,[])

		for i in range(len(self)):
			new_beta.append(self[i]*other)

		return new_beta


	def __imul__(self, other):
		for i in range(len(self)):
			self[i] *= other

		return self

	
	def __add__(self, other):
		if len(self) < len(other):
			o = other.transpose(self.l0)
			s = self.transpose(end=len(other)-1)
		else:
			o = other.transpose(self.l0, end=len(self)-1)
			s = self


		new_beta = Beta(self.l0,[])

		for i in range(len(self)):
			new_beta.append(s[i] + o[i])

		return new_beta
	

	def __iadd__(self, other):
		if len(self) > len(other):
			o = other.transpose(self.l0, end=len(self)-1)
		elif len(self) < len(other):
			o = other.transpose(self.l0)
			for i in range(len(other)-len(self)):
				self.append(0)

		for i in range(len(self)):
			self[i] += o[i]

		return self
	
			
		

	

class ProgressBar(object):
	def __init__(self, N, step=1):
		self.i = 0
		self.N = N
		self.step = step

		print('[{}]'.format('.' * (self.N//self.step)),'\b'*(self.N//self.step+2), end='', flush=True)

	def increment(self):
		if self.i < self.N:
			self.i += 1
			if self.i % self.step == 0:
				print('#', sep='', end='', flush=True)
		else:
			print('\n')


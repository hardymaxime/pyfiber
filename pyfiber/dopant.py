# -*- coding: utf-8 -*-

from scipy.constants import hbar, h, c, pi
import numpy as np

from . import tools, mode_LP

# N.B. ONLY THE FIBER CORE IS DOPED FOR DOUBLE CLAD FIBERS


	
class Dopant(tools.Copyable2):
	def __init__(self, N=1, radius=1, symbol='', sigma={}, tau={}, Nsaves=1, z=0, N1=1, N2=0, **kwargs):
		super().__init__(**kwargs)
		
		self.Nsaves = Nsaves

		self.N = N
		self.symbol = symbol
		self.radius = radius

		self.z = tools.Saved(z, self.Nsaves)
		self.N1 = tools.Saved(N1, self.Nsaves)
		self.N2 = tools.Saved(N2, self.Nsaves)

		self.sigma = sigma	# Cross-section functions for self.sigma[i,j]
		self._sigma = {}	# Cross-section evaluated for a sigle beam with self._sigma[(i,i),the_beam]
		self.tau = tau

		self.copy_args += ['N', 'radius', 'symbol', 'sigma', 'tau', 'Nsaves', 'z', 'N1', 'N2']


	def __str__(self):
		return '{} : N = {:0.4g} m^-3'.format(self.symbol, self.N)
	
	

	def save(self):
		self.z.save()
		self.N1.save()
		self.N2.save()

	
	def transfer_rate(self, levels, the_beam, average=True):
		if levels not in self.sigma:
			raise AttributeError('{} dopant does not have a transition between levels {} and {}'\
					.format(self.symbol, levels[0], levels[1]))

		if (levels, the_beam) not in self._sigma:
			self._sigma[levels, the_beam] = self.sigma[levels](the_beam.l)

		sigma = self._sigma[levels, the_beam]

		if average and hasattr(the_beam, 'S'):
			R = np.sum((sigma.real * the_beam.S() * the_beam.rate) / (hbar * (the_beam.w + the_beam.w0)) ) * the_beam .dw
				
		elif average:
			R = (sigma.real * the_beam.Pmean()) / (hbar * the_beam.w0)

		else:
			l = the_beam.l_T(pol='x') if hasattr(the_beam, 'l_T') else the_beam.l0
			R = (np.real(self.sigma[levels](l)) * the_beam.P()) / (hbar * (the_beam.w + the_beam.w0))

		return R * the_beam.mode.confinement / (pi * self.radius**2)
			

		

class TwoLevelDopant(Dopant):
	def __init__(self, Npts=1, dN1=0, **kwargs):
		super().__init__(**kwargs)
		
		assert((1,2) in self.sigma)
		assert((2,1) in self.sigma)
		assert((2,1) in self.tau)
		
		self.Npts = Npts

		if hasattr(dN1, '__len__') and len(dN1) == self.Npts:
			self.dN1 = tools.Saved(dN1, self.Nsaves)
		else:
			self.dN1 = tools.Saved(np.zeros(Npts), self.Nsaves)

		self.dN1_av = tools.Saved(0, self.Nsaves)	# Moyenne sur une période entre deux impulsions

		self.copy_args += ['Npts', 'dN1']


	def save(self):
		super().save()

		self.dN1.save()
		self.dN1_av.save()


	def small_signal_absorption(self, l0, confinement=1):
		return 10 * self.N * self.sigma[1,2](l0).real * confinement * np.log10(np.e)


	def ___find_populations(self, pumps, signals=[]):
		R12, R21 = 0., 0.

		for each_pump in pumps:
			R12 += self.transfer_rate((1,2), each_pump)
			R21 += self.transfer_rate((2,1), each_pump)

		self.N1.set((1 + R21*self.tau[2,1]) / (1 + (R12 + R21)*self.tau[2,1]))
		self.N2.set(1 - self.N1())

		if len(signals) > 0:
			W12, W21 = 0., 0.

			for each_signal in signals:
				W12 += self.transfer_rate((1,2), each_signal, average=True)
				W21 += self.transfer_rate((2,1), each_signal, average=True)

			delay = 1/signals[0].rate
			RWT = (R12 + R21 + W12 + W21 + 1/self.tau[2,1]) * delay
			expRWT = np.exp(RWT)

			dN1e = delay * (W21 * self.N2() - W12 * self.N1()) /\
					(np.exp((R12 + R21 + 1/self.tau[2,1])*delay) - 1 + delay * (W12 + W21))
			#dN1e = delay * (W21 * self.N2() - W12 * self.N1()) / (expRWT - 1)

			self.N1 += dN1e
			self.N2 -= dN1e

			N1, N2 = self.N1(), self.N2()

			W12 = np.zeros(len(signals[0].T))
			W21 = np.zeros(len(signals[0].T))
			
			for each_signal in signals:
				W12 += self.transfer_rate((1,2), each_signal, average=False)
				W21 += self.transfer_rate((2,1), each_signal, average=False)


			f = W12 + W21 + R12 + R21 + 1/self.tau[2,1]
			g = W21 * N2 - W12 * N1
			self.dN1.set(tools.first_order_linear_diff_eq(signals[0].T, f, g))

			self.dN1_av.set(self.dN1[-2] * (1 - 1/expRWT) / RWT) 	# 1/expRWT = exp(-RWT)




	def __find_populations(self, pumps, signals=[]):
		R12, R21 = 0., 0.

		for each_pump in pumps:
			R12 += self.transfer_rate((1,2), each_pump)
			R21 += self.transfer_rate((2,1), each_pump)

		self.N1.set((1 + R21*self.tau[2,1]) / (1 + (R12 + R21)*self.tau[2,1]))
		self.N2.set(1 - self.N1())

		if len(signals) > 0:
			#N1, N2 = self.N * self.N1(), self.N * self.N2()
			N1, N2 = self.N1(), self.N2()
			W12a, W21a = 0., 0.
			W12 = np.zeros(len(signals[0].T))
			W21 = np.zeros(len(signals[0].T))

			for each_signal in signals:
				W12a += self.transfer_rate((1,2), each_signal, average=True)
				W21a += self.transfer_rate((2,1), each_signal, average=True)

				W12 += self.transfer_rate((1,2), each_signal, average=False)
				W21 += self.transfer_rate((2,1), each_signal, average=False)

			delay = 1/signals[0].rate
			RWT = (R12 + R21 + W12a + W21a + 1/self.tau[2,1]) * delay
			expRWT = np.exp(RWT)
			DN1 = delay * (W21a * N2 - W12a * N1)

			dN1e = DN1 / (expRWT - 1)

			f = W12 + W21 + R12 + R21 + 1/self.tau[2,1]
			g = W21 * N2 - W12 * N1
			self.dN1.set(tools.first_order_linear_diff_eq(signals[0].T, f, g, dN1e))

			self.dN1_av.set(self.dN1[-2] * (1 - 1/expRWT) / RWT) 	# 1/expRWT = exp(-RWT)
			

			
	def _find_populations(self, pumps, signals=[]):
		R12, R21 = 0., 0.

		for each_pump in pumps:
			R12 += self.transfer_rate((1,2), each_pump)
			R21 += self.transfer_rate((2,1), each_pump)

		self.N1.set((1 + R21*self.tau[2,1]) / (1 + (R12 + R21)*self.tau[2,1]))
		self.N2.set(1 - self.N1())

		if len(signals) > 0:
			#N1, N2 = self.N * self.N1(), self.N * self.N2()
			N1, N2 = self.N1(), self.N2()
			W12a, W21a = 0., 0.
			W12 = np.zeros(len(signals[0].T))
			W21 = np.zeros(len(signals[0].T))

			for each_signal in signals:
				W12a += self.transfer_rate((1,2), each_signal, average=True)
				W21a += self.transfer_rate((2,1), each_signal, average=True)

				W12 += self.transfer_rate((1,2), each_signal, average=False)
				W21 += self.transfer_rate((2,1), each_signal, average=False)

			delay = 1/signals[0].rate

			RT = (R12 + R21 + 1/self.tau[2,1]) * delay
			WT = (W12a + W21a) * delay

			expRT = np.exp(-RT)
			expWT = np.exp(-WT)
			expWT2 = np.exp(-WT/2)

			DN1 = delay * (W21a * N2 - W12a * N1)
			
			dN1e = DN1 * expRT * expWT2 / (1 - expRT * expWT)

			f = W12 + W21 + R12 + R21 + 1/self.tau[2,1]
			g = W21 * N2 - W12 * N1
			self.dN1.set(tools.first_order_linear_diff_eq(signals[0].T, f, g, dN1e))

			self.dN1_av.set((dN1e*expWT + DN1*expWT2) * (1 - expRT) / RT) 	# 1/expRWT = exp(-RWT)
			#print(dN1e*expWT + DN1*expWT2, self.dN1[-2]) 	# Pour vérifier si l'estimation correspond à l'intégration numérique
			

	def find_populations(self, pumps, signals=[]):
		R12, R21 = 0., 0.

		for each_pump in pumps:
			R12 += self.transfer_rate((1,2), each_pump)
			R21 += self.transfer_rate((2,1), each_pump)

		self.N1.set((1 + R21*self.tau[2,1]) / (1 + (R12 + R21)*self.tau[2,1]))
		self.N2.set(1 - self.N1())

		if len(signals) > 0:
			#N1, N2 = self.N * self.N1(), self.N * self.N2()
			N1, N2 = self.N1(), self.N2()
			W12a, W21a = 0., 0.
			W12 = np.zeros(len(signals[0].T),dtype='float64') # Car 1 nombre complexe * 1j float64 = complex128
			W21 = np.zeros(len(signals[0].T),dtype='float64')

			for each_signal in signals:
				W12a += self.transfer_rate((1,2), each_signal, average=True)
				W21a += self.transfer_rate((2,1), each_signal, average=True)

				W12 += self.transfer_rate((1,2), each_signal, average=False)
				W21 += self.transfer_rate((2,1), each_signal, average=False)

			delay = 1/signals[0].rate

			R = (R12 + R21 + 1/self.tau[2,1])
			W = (W12a + W21a)
			expRT = np.exp(-R * delay)
			expWT = np.exp(-W * delay)

			t = signals[0].T - signals[0].T[0]
			dN1 = tools.first_order_linear_diff_eq(t, W12 + W21, W21 * N2 - W12 * N1)
			DN1 = dN1[-2] - dN1[0]

			dN1e = DN1 * expRT / (1 - expRT * expWT)

			#expWt = tools.first_order_linear_diff_eq(t, W12 + W21, np.zeros(W12.shape), 1)
			#self.dN1.set((dN1e * expWt + dN1) * np.exp(-R*t))
			self.dN1.set(tools.first_order_linear_diff_eq(t, W12 + W21 + R, W21 * N2 - W12*N1, dN1e))
			#print(self.dN1[0], self.dN1[-2])
			self.dN1_av.set((dN1e*expWT + DN1) * (1 - expRT) / (R*delay))
			


	
	def gain(self, the_beam):	# With populations at equilibrium
		'''This gives the average gain for each dopant atom.
		One must multiply its value by the dopant concentration'''
		for levels in [(1,2), (2,1)]:
			if (levels, the_beam) not in self._sigma:
				self._sigma[levels, the_beam] = self.sigma[levels](the_beam.l)

		gain = self._sigma[(2,1), the_beam] * self.N2() - self._sigma[(1,2), the_beam] * self.N1()
			

		return 0.5 * self.N * gain * the_beam.mode.confinement

	
	def gain_saturation(self, the_beam):
		'''This gives the substraction to the equilibrium gain given by method "gain"
		that must be calculated in the time domain to include gain saturation'''

		l = the_beam.l_T(pol='x') if hasattr(the_beam, 'l_T') else the_beam.l0
		sigma = self.sigma[1,2](l) + self.sigma[2,1](l)
		#dN1 = self.dN1() if hasattr(the_beam, 'T') else self.dN1[0]	# Faisceaux continus non affectés par la saturation
		#dN1 = self.dN1() if hasattr(the_beam, 'T') else self.dN1[-2]	# Faisceaux continus très affectés par la saturation
		dN1 = self.dN1() if hasattr(the_beam, 'T') else self.dN1_av()	# Faisceaux continus affectés par la saturation moyenne

		return -0.5 * self.N * sigma * dN1 * the_beam.mode.confinement
		

		
class ThreeLevelDopant(Dopant):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)

		assert((1,2) in self.sigma)
		assert((2,1) in self.sigma)
		assert((1,3) in self.sigma)
		assert((2,1) in self.tau)


	def small_signal_absorption(self, l0, confinement=1):
		return 10 * self.N * (self.sigma[1,2](l0).real + self.sigma[1,3](l0).real) * confinement * np.log10(np.e)


	def find_populations(self, fiber, beams):
		'''Find normalized populations'''
		W12, W21, R13 = 0., 0., 0.

		for each_beam in beams:
			W12 += self.transfer_rate((1,2), each_beam)
			W21 += self.transfer_rate((2,1), each_beam)
			R13 += self.transfer_rate((1,3), each_beam)
		

		self.N1.set((1 + W21*self.tau[2,1]) / (1 + (W12 + W21 + R13)*self.tau[2,1]))
		self.N2.set(1 - self.N1())



	def gain(self, the_beam):
		for levels in [(1,2), (2,1), (1,3)]:
			if (levels, the_beam) not in self._sigma:
				self._sigma[levels, the_beam] = self.sigma[levels](the_beam.l)

		#s12 = self._sigma.setdefault(((1,2), the_beam), self.sigma[1,2](the_beam.l))
		#s21 = self._sigma.setdefault(((2,1), the_beam), self.sigma[2,1](the_beam.l))
		#s13  = self._sigma.setdefault(((1,3), the_beam), self.sigma[1,3](the_beam.l))

		gain12 = self._sigma[(2,1), the_beam] * self.N2() - self._sigma[(1,2), the_beam] * self.N1()
		gain13 = - self._sigma[(1,3), the_beam] * self.N1()

		return 0.5 * self.N * (gain12 + gain13) * the_beam.mode.confinement



class FourLevelDopant(Dopant):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)

		assert((1,2) in self.sigma)
		assert((2,1) in self.sigma)
		assert((0,3) in self.sigma)


	def small_signal_absorption(self, l0, confinement=1):
		return 10 * self.N * self.sigma[0,3](l0).real * confinement * np.log10(np.e)


	def find_populations(self, fiber, beams):
		'''Find normalized populations'''
		W12, W21, R03 = 0., 0., 0.

		for each_beam in beams:
			W12 += self.transfer_rate((1,2), each_beam)
			W21 += self.transfer_rate((2,1), each_beam)
			R03 += self.transfer_rate((0,3), each_beam)

		denominator = 1 + W21*self.tau[2,1] + R03*self.tau[2,1] * (1 + (W12+W21)*self.tau[1,0] + self.tau[1,0]/self.tau[2,1])
		
		self.N1.set(R03 * self.tau[1,0] * (1 + W21*self.tau[2,1]) / denominator)
		self.N2.set(R03 * self.tau[2,1] * (1 + W12*self.tau[1,0]) / denominator)



	def gain(self, the_beam):
		for levels in [(1,2), (2,1), (0,3)]:
			if (levels, the_beam) not in self._sigma:
				self._sigma[levels, the_beam] = self.sigma[levels](the_beam.l)

		#s12 = self._sigma.setdefault(((1,2), the_beam), self.sigma[1,2](the_beam.l))
		#s21 = self._sigma.setdefault(((2,1), the_beam), self.sigma[2,1](the_beam.l))
		#s03  = self._sigma.setdefault(((0,3), the_beam), self.sigma[0,3](the_beam.l))

		gain12 = self._sigma[(2,1), the_beam] * self.N2() - self._sigma[(1,2), the_beam] * self.N1()
		gain03 = - self._sigma[(0,3), the_beam] * (1 - self.N1() - self.N2())

		return 0.5 * self.N * (gain12 + gain03) * the_beam.mode.confinement



############################# DOPANTS ###############################################################

def _Er_sigma12(l):
	s12 = 3.45e-25 * tools.complex_lorentzian(l,1529e-9,17e-9)
	s12 += 2.2e-25 * tools.complex_gaussian(l,1493e-9,30e-9)
	s12 += 1.4e-25 * tools.complex_lorentzian(l,1552e-9,32e-9)
	return s12


def _Er_sigma21(l):
	s21 = 3.55e-25 * tools.complex_lorentzian(l,1529.5e-9,17e-9)
	s21 += 0.28e-25 * tools.complex_gaussian(l,1544e-9,4e-9)
	s21 += 2.3e-25 * tools.complex_gaussian(l,1556e-9,14e-9)
	s21 += 0.8e-25 * tools.complex_gaussian(l,1594e-9,22e-9)
	s21 += 1e-25 * tools.complex_gaussian(l,1507e-9,27e-9)
	return s21
	

def _Er_sigma13(l):
	s13 = 1.9e-25 * tools.complex_gaussian(l,980e-9,10e-9)
	return s13


def _Yb_sigma12(l):
	s12 = 2600e-27 * tools.complex_lorentzian(l,975e-9,5.5e-9)
	s12 += 830e-27 * tools.complex_gaussian(l,911e-9,22e-9)
	s12 += 70e-27 * tools.complex_gaussian(l,1010e-9,16e-9)
	s12 += 80e-27 * tools.complex_gaussian(l,958e-9,10e-9)
	return s12


def _Yb_sigma21(l):
	s21 = 2600e-27 * tools.complex_lorentzian(l,975e-9,5.5e-9)
	s21 += 475e-27 * tools.complex_gaussian(l,1024e-9,18e-9)
	s21 += 40e-27 * tools.complex_gaussian(l,930e-9,16e-9)
	s21 += 250e-27 * tools.complex_gaussian(l,1065e-9,40e-9)
	return s21


def _Nd_sigma12(l):
	s12 = 2.7e-24 * tools.complex_lorentzian(l,1064e-9,14e-9)
	return s12


def _Nd_sigma21(l):
	s21 = 2.7e-24 * tools.complex_lorentzian(l,1064e-9,14e-9)
	return s21


def _Nd_sigma03(l):
	s03 = 1e-24 * tools.complex_lorentzian(l,860e-9,10e-9)
	return s03


class Erbium(ThreeLevelDopant):
	def __init__(self, **kwargs):
		kwargs.update(dict(symbol='Er', tau={(2,1):10e-3}, sigma={(1,2):_Er_sigma12, (2,1):_Er_sigma21, (1,3):_Er_sigma13}))
		super().__init__(**kwargs)


class Ytterbium(TwoLevelDopant):
	def __init__(self, **kwargs):
		kwargs.update(dict(symbol='Yb', tau={(2,1):0.8e-3}, sigma={(1,2):_Yb_sigma12, (2,1):_Yb_sigma21}))
		super().__init__(**kwargs)


class Neodymium(FourLevelDopant):
	def __init__(self, **kwargs):
		kwargs.update(dict(symbol='Nd', tau={(1,0):635e-12, (2,1):340e-6},\
				sigma={(1,2):_Nd_sigma12, (2,1):_Nd_sigma21, (0,3):_Nd_sigma03}))

		super().__init__(**kwargs)

		#self.tau1 = 635e-12	# Bibeau1981





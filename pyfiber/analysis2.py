# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.constants import pi

from . import tools, beam

def surface(self,x,y,z,cstride=5,rstride=2):
	X,Y = np.meshgrid(x,y)
	return self.plot_surface(X,Y,z,cmap=mpl.cm.jet,cstride=cstride,rstride=rstride,linewidth=0,antialiased=False,shade=False)

Axes3D.surface = surface


def show_fwhm(pulse, ax=None, unit={}, T0=0., text_height=0.85, fontsize=14, lw=1.5):
	if ax is None:
		ax = mpl.pyplot.gca()

	unit = tools.UnitDict(unit)
	
	m = np.argmax(pulse.P())
	h1 = np.argmin(abs(0.5*pulse.Pmax() - pulse.P()[0:m]))
	h2 = m + np.argmin(abs(0.5*pulse.Pmax() - pulse.P()[m:-1]))
	DT = pulse.DT(FWHM=True)

	ax.annotate('', ((pulse.T[h1]-T0)/unit['T'], pulse.P()[h1]/unit['P']), ((pulse.T[h2]-T0)/unit['T'], pulse.P()[h1]/unit['P']),\
			xycoords='data', arrowprops={'arrowstyle':'<->','lw':lw})

	ax.text((pulse.T[m]-T0)/unit['T'], text_height*pulse.P()[h1]/unit['P'],\
			'{:.0f} {}s'.format(DT/unit['T'],unit.prefix('T')), horizontalalignment='center', fontsize=fontsize)

	





class Analysis(object):
	def __init__(self, lim={}, unit={}, labels={}, **ax_kwargs):
		self.fig = mpl.pyplot.figure()

		self.lim = dict(lim)
		self.unit = tools.UnitDict(unit)
		self.labels = dict(labels)

		self.ax_kwargs = ax_kwargs
		self.axes = {}


	def ax(self, *loc, **kwargs):
		if loc not in self.axes:
			kwargs.update(self.ax_kwargs)
			self.axes[loc] = self.fig.add_subplot(*loc, **kwargs)

		return self.axes[loc]


	def summary(self, x, y, loc=(1,1,1), x_key='', y_key='', center=False, normalized=False, legend_loc=None, *args, **kwargs):
		ax = self.ax(*loc)

		if center:
			x = x - tools.wmean(x,y) # Cannot be x -= tools.wmean(x,y) because we don't want original 'x' to be modified
		
		if normalized:
			y = y/np.amax(y)
			self.unit[y_key] = 1.

		ax.plot(x/self.unit[x_key], y/self.unit[y_key], '.-', *args, **kwargs)

		if x_key not in self.lim:
			xm = tools.wmean(x,y)
			sigma = tools.wstd(x,y)
			self.lim[x_key] = (xm - 6*sigma, xm + 6*sigma)

		ax.set_xlim(*(limit/self.unit[x_key] for limit in self.lim[x_key]))

		if y_key in self.lim:
			ax.set_ylim(*(limit/self.unit[y_key] for limit in self.lim[y_key]))

		
		if x_key in self.labels:
			ax.set_xlabel(self.labels[x_key].format(self.unit.prefix(x_key)))

		if y_key in self.labels:
			ax.set_ylabel(self.labels[y_key].format(self.unit.prefix(y_key)))

		if legend_loc is not None:
			mpl.pyplot.legend(loc=legend_loc,frameon=False,prop={'size':10})

		return ax


	
	def history(self, x, y, loc=(1,1,1), x_key='', y_key='', legend_loc=None, *args, **kwargs):
		ax = self.ax(*loc)

		ax.plot(x/self.unit[x_key], y/self.unit[y_key], '.-', *args, **kwargs)

		if x_key in self.lim:
			ax.set_xlim(*(limit/self.unit[x_key] for limit in self.lim[x_key]))

		if y_key in self.lim:
			ax.set_ylim(*(limit/self.unit[y_key] for limit in self.lim[y_key]))

		if x_key in self.labels:
			ax.set_xlabel(self.labels[x_key].format(self.unit.prefix(x_key)))

		if y_key in self.labels:
			ax.set_ylabel(self.labels[y_key].format(self.unit.prefix(y_key)))

		if legend_loc is not None:
			mpl.pyplot.legend(loc=legend_loc,frameon=False,prop={'size':10})

		return ax



	def history_3d(self, x, y, z, loc=(1,1,1), x_key='', y_key='', z_key='', *args, **kwargs):
		ax = self.ax(*loc, projection='3d')

		if x_key not in self.lim:
			xm = tools.wmean(x,z)
			sigma = np.amax(tools.wstd(x,z,axis=-1))
			self.lim[x_key] = (xm - 2*sigma, xm + 2*sigma)

		xlim = tools.limit(x,*self.lim[x_key])
		ylim = tools.limit(y,*self.lim[y_key]) if y_key in self.lim else slice(None)

		ax.surface(x[xlim]/self.unit[x_key], y[ylim]/self.unit[y_key], z[ylim,xlim]/self.unit[z_key], *args, **kwargs)
		
		if x_key in self.labels:
			ax.set_xlabel(self.labels[x_key].format(self.unit.prefix(x_key)))
		
		if y_key in self.labels:
			ax.set_ylabel(self.labels[y_key].format(self.unit.prefix(y_key)))

		if z_key in self.labels:
			ax.set_zlabel(self.labels[z_key].format(self.unit.prefix(z_key)))

		return ax






############################################################################



def pulse_summary(*beams, pol='x', lim={}, unit={}, labels={}, color_cycle=['k','b','r','g','c','m','y'], **kwargs):
	_lim = {'AC':(0.,1.)}
	_lim.update(lim)

	_unit = tools.UnitDict({'T':1e-12, 'T_AC':1e-12, 'l':1e-9, 'chirp':1e12})
	_unit.adjust({'P':max([each_beam.Pmax() for each_beam in beams])})
	_unit.update(unit)

	_labels = {'T':'$T$ ({}s)', 'l':'$\lambda$ ({}m)', 'P':'$P$ ({}W)', 'S':'$S$ ({}J s/rad)',\
			'chirp':'$\mathrm{{d}}\phi/\mathrm{{d}}T$ ({}Hz)', 'T_AC':'$T$ ({}s)', 'AC':'$P \star P$'} 
	_labels.update(labels)

	s = Analysis(_lim, _unit, _labels, color_cycle=color_cycle)

	for each_beam in beams:
		the_beam = each_beam.copy(pol=pol, **kwargs)
		print(the_beam.info())

		if pol is None:
			beam_x = each_beam.copy(pol='x', **kwargs)
			beam_y = each_beam.copy(pol='y', **kwargs)

			s.summary(the_beam.T, beam_x.P(), (2,2,1), x_key='T', y_key='P')
			s.summary(the_beam.T, beam_y.P(), (2,2,1), x_key='T', y_key='P')
			s.summary(the_beam.T, the_beam.P(), (2,2,1), x_key='T', y_key='P')

			s.summary(the_beam.l, beam_x.S(), (2,2,2), x_key='l', y_key='S')
			s.summary(the_beam.l, beam_y.S(), (2,2,2), x_key='l', y_key='S')
			s.summary(the_beam.l, the_beam.S(), (2,2,2), x_key='l', y_key='S')

			s.summary(the_beam.T, beam_x.chirp()/(2*pi), (2,2,3), x_key='T', y_key='chirp')
			s.summary(the_beam.T, beam_y.chirp()/(2*pi), (2,2,3), x_key='T', y_key='chirp')

			s.summary(the_beam.T, the_beam.autocorr(), (2,2,4), x_key='T_AC', y_key='AC', center=True)

	
		else:
			s.summary(the_beam.T, the_beam.P(), (2,2,1), x_key='T', y_key='P')
			s.summary(the_beam.l, the_beam.S(), (2,2,2), x_key='l', y_key='S')
			s.summary(the_beam.T, the_beam.chirp()/(2*pi), (2,2,3), x_key='T', y_key='chirp')
			s.summary(the_beam.T, the_beam.autocorr(), (2,2,4), x_key='T_AC', y_key='AC', center=True)

	return s



def Stokes_summary(the_beam, verbose=True, lim={}, unit={}, labels={}, **kwargs):
	_unit = {'T':1e-12, 'Stokes':1}
	_unit.update(unit)

	_labels = {'T':'$T$ ({}s)', 'Stokes':'Stokes parameters ({}W)'} 
	_labels.update(labels)

	s = Analysis(lim, _unit, _labels)

	stokes = the_beam.copy(**kwargs).stokes()

	s.summary(the_beam.T, stokes[0], (1,1,1), x_key='T', y_key='Stokes', color='r', label='Intensity')
	s.summary(the_beam.T, -stokes[0], (1,1,1), x_key='T', y_key='Stokes', color='r')

	s.summary(the_beam.T, stokes[1], (1,1,1), x_key='T', y_key='Stokes', color='b', label='Horizontal/Vertical')
	s.summary(the_beam.T, stokes[2], (1,1,1), x_key='T', y_key='Stokes', color='g', label='+45\degree/-45\degree')
	s.summary(the_beam.T, stokes[3], (1,1,1), x_key='T', y_key='Stokes', color='k', label='Right/Left',legend_loc='upper right')

	return s


def dBm_summary(the_beam, lim={}, unit={}, labels={}, **kwargs):
	_unit = {'T':1e-12, 'T_AC':1e-12, 'f':1e12, 'T_AC':1e-12}
	_unit.update(unit)

	_labels = {'T':'$T$ ({}s)', 'f':'$f$ ({}Hz)', 'P':'$P$ (dBm)', 'S':'$S$ ({}J/Hz)',\
			'chirp':'$\mathrm{{d}}\phi/\mathrm{{d}}T$', 'T_AC':'$T$ ({}s)', 'AC':'$P \star P$'} 
	_labels.update(labels)


	s = Analysis(lim, _unit, _labels)

	s.summary(the_beam.T, tools.P_to_dB(the_beam.copy(**kwargs).P()), (1,1,1), x_key='T', y_key='P')
	
	return s


def raman_gain_summary(amp, signal, *pumps, pol='x', lim={}, unit={}, labels={}):
	_lim = {'l':(signal.carrier_l0() - 3*signal.Dl(), signal.carrier_l0() + 3*signal.Dl())}
	_lim.update(lim)

	_unit = {'l':1e-9}
	_unit.update(unit)
	
	_labels = {'l':'$\lambda$ ({}m)', 'gain':'$g_R$', 'phase':'$\phi_R$'}
	_labels.update(labels)

	s = Analysis(_lim, _unit, _labels)

	raman = amp.Raman_gain_spectrum(signal, *pumps)
	s.summary(signal.l, raman.real, (2,1,1), x_key='l', y_key='gain')
	s.summary(signal.l, raman.imag, (2,1,2), x_key='l', y_key='phase')

	for each_pump in pumps:
		raman = amp.Raman_gain_spectrum(signal,each_pump)
		s.summary(signal.l, raman.real, (2,1,1), x_key='l', y_key='gain')
		s.summary(signal.l, raman.imag, (2,1,2), x_key='l', y_key='phase')

	return s


############################################################################


def propagation_history(*beams, lim={}, unit={}, labels={}, color_cycle=['k','b','r','g','c','m','y'], **kwargs):
	_unit = {'Pmax':1e3, 'E':1e-9, 'DT':1e-12, 'Dl':1e-9, 'Pmean':1e-3, 'l0':1e-6}
	_unit.update(unit)

	_labels = {'z':'$z$ ({}m)', 'E':'$E$ ({}J)', 'Pmax':'$P_\mathrm{{max}}$ ({}W)', 'DT':'$\Delta T$ ({}s)',\
			'Dl':'$\Delta \lambda$ ({}m)', 'Pmean':'$\langle P \\rangle$ ({}W)', 'l0':'$\lambda_0$ ({}m)'}
	_labels.update(labels)

	h = Analysis(lim, _unit, _labels, color_cycle=color_cycle)

	for each_beam in beams:
		the_beam = each_beam.copy(i='all', **kwargs)

		h.history(the_beam.z('all'), the_beam.Pmax('all'), (2,2,1), x_key='z', y_key='Pmax')
		h.history(the_beam.z('all'), the_beam.energy('all'), (2,2,2), x_key='z', y_key='E')
		h.history(the_beam.z('all'), the_beam.DT('all'), (2,2,3), x_key='z', y_key='DT')
		h.history(the_beam.z('all'), the_beam.Dl('all'), (2,2,4), x_key='z', y_key='Dl')

	return h


def soliton_history(*beams, lim={}, unit={}, labels={}, color_cycle=['k','b','r','g','c','m','y'], **kwargs):
	_unit = {'Pmax':1e3, 'DT':1e-12, 'DT_FWHM':1e-12, 'Dl':1e-9, 'l0':1e-6}
	_unit.update(unit)

	_labels = {'z':'$z$ ({}m)', 'Pmax':'$P_\mathrm{{max}}$ ({}W)', 'DT':'$\Delta T$ ({}s)',\
			'DT_FWHM':'$\Delta T_\mathrm{{FWHM}}$ ({}s)', 'Dl_FWHM':'$\Delta \lambda_\mathrm{{FWHM}}$ ({}m)',\
			'Dl':'$\Delta \lambda$ ({}m)', 'Pmean':'$\langle P \\rangle$ ({}W)', 'l0':'$\lambda_0$ ({}m)'}
	_labels.update(labels)

	h = Analysis(lim, _unit, _labels, color_cycle=color_cycle)

	for each_beam in beams:
		the_beam = each_beam.copy(i='all', **kwargs)

		h.history(the_beam.z('all'), the_beam.Pmax('all'), (2,2,1), x_key='z', y_key='Pmax')
		h.history(the_beam.z('all'), the_beam.carrier_l0('all'), (2,2,2), x_key='z', y_key='l0')
		h.history(the_beam.z('all'), the_beam.DT('all',FWHM=True), (2,2,3), x_key='z', y_key='DT_FWHM')
		h.history(the_beam.z('all'), the_beam.Dl('all',FWHM=True), (2,2,4), x_key='z', y_key='Dl_FWHM')

	return h




def ddl_history(dispersive_delay_line, lim={}, unit={}, labels={}, color_cycle=['k','b','r','g','c','m','y'], **kwargs):
	if 'pol' not in kwargs:
		kwargs['pol'] = 'x'

	_unit = {'DT':1e-12, 'DT_FWHM':1e-12}
	_unit.update(unit)

	_labels = {'z':'$z$', 'Pmax_E':'$P_\mathrm{{max}}/E$ ({}W/J)',\
			'DT':'$\Delta T$ ({}s)', 'DT_FWHM':'$\Delta T_\mathrm{{FWHM}}$ ({}s)'}
	_labels.update(labels)


	h = Analysis(lim, _unit, _labels, color_cycle=color_cycle)

	for each_beam in dispersive_delay_line.beam_list:
		the_beam = each_beam.copy(i='all', **kwargs)

		h.history(the_beam.z('all'), the_beam.Pmax('all')/the_beam.energy('all'), (2,1,1), x_key='z', y_key='Pmax_E')
		h.history(the_beam.z('all'), the_beam.DT('all',FWHM=False), (2,2,3), x_key='z', y_key='DT')
		h.history(the_beam.z('all'), the_beam.DT('all',FWHM=True), (2,2,4), x_key='z', y_key='DT_FWHM')

	h.axes[2,1,1].set_title('$\\beta_i = $ {}'.format(dispersive_delay_line.beta.__repr__()))

	return h



def amplification__history(amp, lim={}, unit={}, labels={}, color_cycle=['k','b','r','g','c','m','y']):
	_lim = {'levels':(0,1)}
	_lim.update(lim)

	_unit = {'Pmax':1e3, 'E':1e-9, 'DT':1e-12, 'Dl':1e-9, 'Pmean':1, 'l0':1e-6}
	_unit.update(unit)

	_labels = {'z':'$z$ ({}m)', 'E':'$E$ ({}J)', 'Pmax':'$P_\mathrm{{max}}$ ({}W)', 'DT':'$\Delta T$ ({}s)',\
			'Dl':'$\Delta \lambda$ ({}m)', 'Pmean':'$\langle P \\rangle$ ({}W)', 'l0':'$\lambda_0$ ({}m)',\
			'Pmean':'$\langle P \\rangle$ ({}W)', 'levels':'Normalized population', 'l0':'$\lambda_0$ ({}m)'}
	_labels.update(labels)

	h = Analysis(_lim, _unit, _labels, color_cycle=color_cycle)

	for each_beam in amp.signal_list:
		the_beam = each_beam.copy(i='all', **kwargs)

		h.history(the_beam.z('all'), the_beam.Pmax('all'), (2,2,1), x_key='z', y_key='Pmax')
		h.history(the_beam.z('all'), the_beam.energy('all'), (4,2,2), x_key='z', y_key='E')
		h.history(the_beam.z('all'), the_beam.DT('all'), (4,2,5), x_key='z', y_key='DT')
		h.history(the_beam.z('all'), the_beam.Dl('all'), (4,2,7), x_key='z', y_key='Dl')
		h.history(the_beam.z('all'), the_beam.Pmean('all'), (4,2,4), x_key='z', y_key='Pmean')
		h.history(the_beam.z('all'), the_beam.carrier_l0('all'), (4,2,6), x_key='z', y_key='l0')
	
	for each_beam in amp.pump_list:
		the_beam = each_beam.copy(i='all', **kwargs)
		h.history(the_beam.z('all'), the_beam.Pmean('all'), (4,2,4), x_key='z', y_key='Pmean')

	for each_dopant in amp.dopants.values():
		N1 = each_dopant.N1('all')
		N2 = each_dopant.N2('all')

		if hasattr(each_dopant, 'dN1'):
			h.history(each_dopant.z('all'), N1, (4,2,8), x_key='z', y_key='levels', color='gray')
			h.history(each_dopant.z('all'), N2, (4,2,8), x_key='z', y_key='levels', color='gray')

			N1 += each_dopant.dN1('all').T[-2]
			N2 -= each_dopant.dN1('all').T[-2]

		
		h.history(each_dopant.z('all'), N1, (4,2,8), x_key='z', y_key='levels', label='$N_1$ {}'.format(each_dopant.symbol))
		h.history(each_dopant.z('all'), N2, (4,2,8), x_key='z', y_key='levels', label='$N_2$ {}'.format(each_dopant.symbol),\
				legend_loc='upper_right')

	return h


def amplification_saturation_summary(amp, lim={}, unit={}, labels={}, color_cycle=['k','b','r','g','c','m','y'], **kwargs):
	T = amp.signal_list[0].T

	_lim = {'T':(T[0],T[-1]), 'Pop':(0,1)}
	_lim.update(lim)

	_unit = {'l':1e-9, 'T':1e-9, 'Gain':1}
	_unit.update(unit)
	
	_labels = {'T':'$T$ ({}s)', 'Pop':'Populations', 'Gain':'Gain ({}$\metre^{{-1}}$)'}
	_labels.update(labels)

	s = Analysis(_lim, _unit, _labels)
	
	for each_dopant in amp.dopants.values():
		s.summary(T, np.array([each_dopant.N1()]*each_dopant.Npts), (2,1,1), x_key='T', y_key='Pop', color='gray')
		s.summary(T, np.array([each_dopant.N2()]*each_dopant.Npts), (2,1,1), x_key='T', y_key='Pop', color='gray')
		s.summary(T, each_dopant.N1() + each_dopant.dN1(), (2,1,1), x_key='T', y_key='Pop', label='N1')
		s.summary(T, each_dopant.N2() - each_dopant.dN1(), (2,1,1), x_key='T', y_key='Pop', label='N2', legend_loc='best')
		
		for each_signal in amp.signal_list:
			the_signal = each_signal.copy(**kwargs)
			l = the_signal.l_T(pol='x') if hasattr(the_signal, 'l_T') else the_signal.l0

			gain_e = (each_dopant.sigma21(l)*each_dopant.N2() - each_dopant.sigma12(l)*each_dopant.N1()) \
					* the_signal.mode.confinement * each_dopant.N

			gain = gain_e - (each_dopant.sigma21(l) + each_dopant.sigma12(l)) * each_dopant.dN1() \
					* the_signal.mode.confinement * each_dopant.N

			s.summary(T, gain_e.real, (2,1,2), x_key='T', y_key='Gain', color='gray')
			s.summary(T, gain.real, (2,1,2), x_key='T', y_key='Gain',\
					label='$\lambda_0 =$ {:0.0f}{}m'.format(the_signal.carrier_l0(**kwargs)/s.unit['l'],s.unit.prefix('l')),\
					legend_loc='best')

	return s



def cavity_history(the_cavity, pol=None, lim={}, unit={}, labels={}, color_cycle=['k','b','r'], **kwargs):
	_unit = {'Pmax':1, 'E':1e-9, 'DT':1e-12, 'Dl':1e-9}
	_unit.update(unit)

	_labels = {'z':'$z$ ({}m)', 'E':'$E$ ({}J)', 'Pmax':'$P_\mathrm{{max}}$ ({}W)',\
			'DT':'$\Delta T$ ({}s)', 'Dl':'$\Delta \lambda$ ({}m)'}
	labels.update(labels)

	h = Analysis(lim, _unit, _labels, color_cycle=color_cycle)

	for i in range(len(the_cavity.beam_list[0])):
		each_beam_list = the_cavity.get_beam(i)

		for each_beam in each_beam_list:
			the_beam = each_beam.copy(i='all', pol=pol, **kwargs)
			if pol is None:
				beam_x = each_beam.copy(i='all', pol='x', **kwargs)
				beam_y = each_beam.copy(i='all', pol='y', **kwargs)

				h.history(each_beam.z('all'), beam_x.Pmax('all'), (2,2,1), x_key='z', y_key='Pmax')
				h.history(each_beam.z('all'), beam_y.Pmax('all'), (2,2,1), x_key='z', y_key='Pmax')
				h.history(each_beam.z('all'), the_beam.Pmax('all'), (2,2,1), x_key='z', y_key='Pmax')

				h.history(each_beam.z('all'), beam_x.energy('all'), (2,2,2), x_key='z', y_key='E')
				h.history(each_beam.z('all'), beam_y.energy('all'), (2,2,2), x_key='z', y_key='E')
				h.history(each_beam.z('all'), the_beam.energy('all'), (2,2,2), x_key='z', y_key='E')

				h.history(each_beam.z('all'), beam_x.DT('all'), (2,2,3), x_key='z', y_key='DT')
				h.history(each_beam.z('all'), beam_y.DT('all'), (2,2,3), x_key='z', y_key='DT')
				h.history(each_beam.z('all'), the_beam.DT('all'), (2,2,3), x_key='z', y_key='DT')

				h.history(each_beam.z('all'), beam_x.Dl('all'), (2,2,4), x_key='z', y_key='Dl')
				h.history(each_beam.z('all'), beam_y.Dl('all'), (2,2,4), x_key='z', y_key='Dl')
				h.history(each_beam.z('all'), the_beam.Dl('all'), (2,2,4), x_key='z', y_key='Dl')
			else:
				h.history(the_beam.z('all'), the_beam.Pmax('all'), (2,2,1), x_key='z', y_key='Pmax')
				h.history(the_beam.z('all'), the_beam.energy('all'), (2,2,2), x_key='z', y_key='E')
				h.history(the_beam.z('all'), the_beam.DT('all'), (2,2,3), x_key='z', y_key='DT')
				h.history(the_beam.z('all'), the_beam.Dl('all'), (2,2,4), x_key='z', y_key='Dl')
	
	return h
				





#def propagation_history_3d(the_beam, pol=None, lim={}, unit={}, labels={}):
#	_unit = {'T':1e-12,'l':1e-9,'Pmax':1e3,'Pmean':1e-3,'DT':1e-12,'Dl':1e-9}
#	_unit.update(unit)
#
#	_lim = {'C':(-100,100)}
#	_lim.update(lim)
#
#	_labels = {'z':'$z$ ({}m)', 'T':'$T$ ({}s)', 'l':'$\lambda$ ({}m)', 'P':'$P$ ({}W)', 'S':'$S$ ({}J/Hz)', \
#			'Pmax':'$P_\mathrm{{max}}$ ({}W)', 'Pmean':'$\langle P \\rangle$ ({}W)', \
#			'DT':'$\Delta T$ ({}s)', 'Dl':'$\Delta \lambda$ ({}m)', 'C':'$C$'}
#	_labels.update(labels)
#
#	h = Analysis(_lim, _unit, _labels)
#
#	# Instant power
#	h.history_3d(the_beam.T, the_beam.z('all'), the_beam.P('all',pol=pol), (2,2,1), x_key='T', y_key='z', z_key='P')
#
#	# Spectrum
#	h.history_3d(the_beam.l, the_beam.z('all'), the_beam.S('all',pol=pol), (2,2,2), x_key='l', y_key='z', z_key='S')
#
#	if pol is None:
#		# DT
#		h.history(the_beam.z('all'), the_beam.DT('all',pol='x',FWHM=True), (4,4,9), x_key='z', y_key='DT')
#		h.history(the_beam.z('all'), the_beam.DT('all',pol='y',FWHM=True), (4,4,9), x_key='z', y_key='DT')
#		h.history(the_beam.z('all'), the_beam.DT('all',pol=None,FWHM=True), (4,4,9), x_key='z', y_key='DT')
#
#		h.history(the_beam.z('all'), the_beam.DT('all',pol='x',FWHM=False), (4,4,13), x_key='z', y_key='DT')
#		h.history(the_beam.z('all'), the_beam.DT('all',pol='y',FWHM=False), (4,4,13), x_key='z', y_key='DT')
#		h.history(the_beam.z('all'), the_beam.DT('all',pol=None,FWHM=False), (4,4,13), x_key='z', y_key='DT')
#
#		# Pmax and Pmean
#		h.history(the_beam.z('all'), the_beam.Pmax('all',pol='x'), (4,4,10), x_key='z', y_key='Pmax')
#		h.history(the_beam.z('all'), the_beam.Pmax('all',pol='y'), (4,4,10), x_key='z', y_key='Pmax')
#		h.history(the_beam.z('all'), the_beam.Pmax('all',pol=None), (4,4,10), x_key='z', y_key='Pmax')
#
#		h.history(the_beam.z('all'), the_beam.Pmean('all',pol='x'), (4,4,14), x_key='z', y_key='Pmean')
#		h.history(the_beam.z('all'), the_beam.Pmean('all',pol='y'), (4,4,14), x_key='z', y_key='Pmean')
#		h.history(the_beam.z('all'), the_beam.Pmean('all',pol=None), (4,4,14), x_key='z', y_key='Pmean')
#
#		# Dl
#		h.history(the_beam.z('all'), the_beam.Dl('all',pol='x',FWHM=True), (4,4,11), x_key='z', y_key='Dl')
#		h.history(the_beam.z('all'), the_beam.Dl('all',pol='y',FWHM=True), (4,4,11), x_key='z', y_key='Dl')
#		h.history(the_beam.z('all'), the_beam.Dl('all',pol=None,FWHM=True), (4,4,11), x_key='z', y_key='Dl')
#
#		h.history(the_beam.z('all'), the_beam.Dl('all',pol='x',FWHM=False), (4,4,15), x_key='z', y_key='Dl')
#		h.history(the_beam.z('all'), the_beam.Dl('all',pol='y',FWHM=False), (4,4,15), x_key='z', y_key='Dl')
#		h.history(the_beam.z('all'), the_beam.Dl('all',pol=None,FWHM=False), (4,4,15), x_key='z', y_key='Dl')
#		
#		# Chirp
#		h.history(the_beam.z('all'), the_beam.C('all',pol='x'), (4,4,12), x_key='z', y_key='C')
#		h.history(the_beam.z('all'), the_beam.C('all',pol='y'), (4,4,12), x_key='z', y_key='C')
#
#	else:
#		h.history(the_beam.z('all'), the_beam.DT('all',pol=pol,FWHM=True), (4,4,9), x_key='z', y_key='DT')
#		h.history(the_beam.z('all'), the_beam.DT('all',pol=pol,FWHM=False), (4,4,13), x_key='z', y_key='DT')
#
#		h.history(the_beam.z('all'), the_beam.Pmax('all',pol=pol), (4,4,10), x_key='z', y_key='Pmax')
#		h.history(the_beam.z('all'), the_beam.Pmean('all',pol=pol), (4,4,14), x_key='z', y_key='Pmean')
#
#		h.history(the_beam.z('all'), the_beam.Dl('all',pol=pol,FWHM=True), (4,4,11), x_key='z', y_key='Dl')
#		h.history(the_beam.z('all'), the_beam.Dl('all',pol=pol,FWHM=False), (4,4,15), x_key='z', y_key='Dl')
#
#		h.history(the_beam.z('all'), the_beam.C('all',pol=pol), (4,4,12), x_key='z', y_key='C')
#
#	return h






			

		
		

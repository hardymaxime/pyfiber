# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import numpy as np
from scipy.constants import hbar, c, pi
from pkg_resources import resource_stream as fopen
from . import beam, tools


# NLS Classes

class Evolution(object):
	def __init__(self, length=1., dz=1e-3, Nsaves=100, progress=False, **kwargs):
		self.length = length
		self.dz = dz
		self.Nsaves = Nsaves
		self.progress = progress


	def __call__(self, *beams, **kwargs):
		self.beam_list = beam.BeamList(beams).copy(Nsaves=self.Nsaves, **kwargs)

		if 'z' not in kwargs:
			for each_beam in self.beam_list:
				each_beam.z.set(0.)

		return self._NLS()

	
	def _NLS_init(self):
		if self.progress:
			self.progress_bar = tools.ProgressBar(self.Nsaves, self.progress)

		self._phase = {}

		for each_beam in self.beam_list:
			self._phase[each_beam] = np.zeros(each_beam.A().shape, dtype='complex64')



	def _NLS_loop_init(self, dz):
		for each_beam in self.beam_list:
			each_beam.z += dz


	def _NLS_time(self, dz):
		for each_beam in self.beam_list:
			each_beam.time_gain(np.exp(self._phase[each_beam]))
			self._phase[each_beam] *= 0


	def _NLS_freq(self, dz):
		for each_beam in self.beam_list:
			each_beam.freq_gain(np.exp(self._phase[each_beam]))
			self._phase[each_beam] *= 0


	def _NLS_save(self, dz):
		for each_beam in self.beam_list:
			each_beam.save()
		
		if self.progress:
			self.progress_bar.increment()


	def _NLS_return(self):
		if self.progress:
			self.progress_bar.increment()

		return self.beam_list

	
	def _NLS(self):
		self._NLS_init()

		Npts = int(self.length/self.dz)
		j = 0

		self._NLS_freq(self.dz/2.)

		for i in range(Npts):
			self._NLS_loop_init(self.dz)
			self._NLS_time(self.dz)

			if i+1 >= j*Npts/(self.Nsaves-1):
				self._NLS_freq(self.dz/2.)
				self._NLS_save(self.dz)
				self._NLS_freq(self.dz/2.)
				j += 1
			else:
				self._NLS_freq(self.dz)

		self._NLS_freq(-self.dz/2.)

		return self._NLS_return()



class LinearEvolution(Evolution):
	def __init__(self, beta, **kwargs):
		self._beta = beta
		super().__init__(**kwargs)


	def _NLS_init(self):
		super()._NLS_init()
		self._dispersion = {}


	def _NLS_freq(self, dz):
		for each_beam in self.beam_list:
			self._phase[each_beam] += self.dispersion(dz, each_beam)

		super()._NLS_freq(dz)


	def dispersion(self, dz, the_beam):
		if the_beam not in self._dispersion:
			beta = self._beta(the_beam)
			self._dispersion[the_beam] = np.array([beta, beta])

		phase = 1j * self._dispersion[the_beam] * dz

		return phase




class DispersiveDelayLine(LinearEvolution):
	def __init__(self, z0, zf, **kwargs):
		self.beta = beta
		self.z0, self.zf = z0, zf
		super().__init__(length=zf-z0, dz=(zf-z0)/Nsaves, **kwargs)


	def _NLS_init(self):
		super()._NLS_init()
			
		self._NLS_freq(self.z0)
		
	


class BeamEvolution(LinearEvolution):
	def __init__(self, fiber, **kwargs):
		self.fiber = fiber
		super().__init__(beta=self.fiber.beta, **kwargs)


	def _NLS_init(self):
		super()._NLS_init()
		self.pulse_list = [p for p in self.beam_list if isinstance(p, beam.PulseTrain)]

		for each_beam in self.beam_list:
			self.fiber.set_mode_attributes(each_beam.l0, each_beam.mode)
			

	def _NLS_time(self, dz):
		for each_beam in self.pulse_list:
			self._phase[each_beam] += self.self_phase_modulation(dz, each_beam)

			for other_beam in (b for b in self.pulse_list if b is not each_beam):
				self._phase[each_beam] += self.cross_phase_modulation(dz, each_beam, other_beam)

		super()._NLS_time(dz)


	def self_phase_modulation(self, dz, the_beam):
		phase = np.zeros(the_beam.A.shape, dtype='complex64')

		phase[0], phase[1] = np.abs(the_beam.A[0])**2, np.abs(the_beam.A[1])**2

		# Relation entre l'automodulation de phase et les différentes polarisations
		phase_power_matrix = np.array([[1, 2], [2, 1]], dtype='complex64') / 3
		
		# Transforme la matrice de puissances en matrice de phase
		phase = np.dot(phase_power_matrix, phase)

		return 2j * self.fiber.gamma(the_beam) * dz * phase
		

	
	def cross_phase_modulation(self, dz, the_beam, other_beam):
		phase = np.zeros(the_beam.A.shape, dtype='complex64')

		phase[0], phase[1] = np.abs(other_beam.A[0])**2, np.abs(other_beam.A[1])**2

		# Relation entre l'automodulation de phase et les différentes polarisations
		phase_power_matrix = np.array([[1, 2], [2, 1]], dtype='complex64') / 3
		
		# Transforme la matrice de puissances en matrice de phase
		phase = np.dot(phase_power_matrix, phase)

		return 4j * self.fiber.gamma(the_beam) * dz * phase




class CounterPropagationBeamEvolution(BeamEvolution):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.counter_propagation()

	def counter_propagation(self, *beams, vg=3e8, **kwargs):
		self.backward_beam_list = beam.BeamList(beams).copy(Nsaves=self.Nsaves, **kwargs)
		self.vg = vg

	def _NLS_init():
		self.beam_list += self.backward_beam_list
		self.forward_beam_list = beam.BeamList([b for b in self.beam_list if b not in self.backward_beam_list])

		super()._NLS_init()
	

	def dispersion(self, dz, the_beam):
		if the_beam in self.backward_beam_list:
			backward_phase = the_beam.w/self.vg
			return np.array([backward_phase, backward_phase]) - super().dispersion(dz, the_beam)
		else:
			return super().dispersion(dz, the_beam)

	
	def self_phase_modulation(self, dz, the_beam):
		sign = -1 if the_beam in self.backward_beam_list else 1
		
		return sign * super().self_phase_modulation(dz, the_beam)


		
	def cross_phase_modulation(self, dz, the_beam, other_beam):
		sign = -1 if the_beam in self.backward_beam_list else 1

		return sign * super().cross_phase_modulation(dz, the_beam, other_beam)


		

class GainTrackEvolution(Evolution):
	def _NLS_init(self):
		super()._NLS_init()

		self.energy_gain = {}

		for each_beam in self.pulse_list:
			self.energy_gain[each_beam,'time'] = tools.Saved(0, Nsaves=self.Nsaves)
			self.energy_gain[each_beam,'freq'] = tools.Saved(0, Nsaves=self.Nsaves)



	
	def _NLS_freq(self, dz):
		energy1, energy2 = {}, {}

		for each_beam in self.pulse_list:
			energy1[each_beam] = each_beam.energy()

		super()._NLS_freq(dz)

		for each_beam in self.pulse_list:
			energy2[each_beam] = each_beam.energy()
			self.energy_gain[each_beam,'freq'] += energy2[each_beam] - energy1[each_beam]

	
	def _NLS_time(self, dz):
		energy1, energy2 = {}, {}

		for each_beam in self.pulse_list:
			energy1[each_beam] = each_beam.energy()

		super()._NLS_time(dz)

		for each_beam in self.pulse_list:
			energy2[each_beam] = each_beam.energy()
			self.energy_gain[each_beam,'time'] += energy2[each_beam] - energy1[each_beam]


	def _NLS_save(self, dz):
		super()._NLS_save(dz)

		for each_beam in self.pulse_list:
			self.energy_gain[each_beam,'time'].save()
			self.energy_gain[each_beam,'freq'].save()


class GainTrackEvolution2(Evolution):
	def _NLS_init(self):
		super()._NLS_init()

		assert(hasattr(self, 'energy_gain_trackers'))
		self.energy_gain = {}
		self._tracked_previous_energy = {}

		for each_tracker in self.energy_gain_trackers:
			for each_beam in self.pulse_list:
				self.energy_gain[each_beam, each_tracker] = tools.Saved(0, Nsaves=self.Nsaves)


	def _NLS_update_tracker(self, tracker, domain):
		assert(domain == 'time' or domain == 'freq')
		
		for each_beam in self.pulse_list:
			energy1 = each_beam.energy()

			if domain == 'time':
				each_beam.time_gain(np.exp(self._phase[each_beam]))
			elif domain == 'freq':
				each_beam.freq_gain(np.exp(self._phase[each_beam]))
			self._phase[each_beam] *= 0

			energy2 = each_beam.energy()

			self.energy_gain[each_beam, tracker] += energy2 - energy1


	def _NLS_save(self, dz):
		super()._NLS_save(dz)
		
		for each_tracker in self.energy_gain_trackers:
			for each_beam in self.pulse_list:
				self.energy_gain[each_beam, each_tracker].save()
			
		

		
		
			




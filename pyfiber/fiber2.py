# -*- coding: utf-8 -*-
import numpy as np
from scipy.constants import hbar, c, pi
from . import mode_LP, tools, dopant


def find_core_radius(cutoff, MFD):
	'''MFD must be a dictionnary with entries being the wavelength associated with the MFD
	For example, MFD={1.55e-6:10.4e-6} or MFD={980e-9:5.9e-6, 1.06e-6:6.2e-6} are correct entries'''
	l0 = np.array(list(MFD.keys()))
	MFD = np.array(list(MFD.values()))

	V = cutoff * 2.405 / l0
	return np.mean(0.5 * MFD / (0.65 + 1.619/V**1.5 + 2.879/V**6.))


class Fiber(tools.Copyable2):
	def __init__(self, cutoff, core_radius, **kwargs):
		super().__init__(**kwargs)

		self.cutoff = cutoff	# cutoff wavelength
		self.core_radius = core_radius
		self.beta = tools.Beta(cutoff, [0.])
		#self.n2 = 2.36e-20
		self.n2 = 2.73e-20 # at 1.06 um d'après Agrawal

		self.dopants = {} # {The dopant symbol : The dopant object}

		self.copy_args += ['cutoff', 'core_radius', 'beta', 'n2', 'dopants']


	def V(self, the_beam):
		return self.cutoff * 2.405 / the_beam.l0


	def set_mode_attributes(self, l0, mode):
		if isinstance(mode, mode_LP.ModeLP):
			mode.set_V(self.cutoff * 2.405 / l0)
			mode.set_radius(self.core_radius)
		else:
			mode.set_radius(self.core_radius)



	def gamma(self, the_beam):
		if not hasattr(the_beam.mode, 'radius'):
			self.set_mode_attributes(the_beam.l0, the_beam.mode)

		radius = self.core_radius * the_beam.mode.radius
		return (2 * self.n2) / (the_beam.l0 * the_beam.mode.radius**2.)



	def dispersion_length(self, the_beam, **kwargs):
		return the_beam.DT(FWHM=True, **kwargs)**2 / ((2 * np.arccosh(np.sqrt(2)))**2 * np.abs(self.beta.transpose(the_beam.l0)[2]))



	def Kerr_length(self, the_beam, **kwargs):
		#return 1 / (self.gamma(the_beam) * the_beam.Pmax(**kwargs))
		return the_beam.DT(FWHM=True,**kwargs) / (self.gamma(the_beam) * the_beam.energy(**kwargs) * np.arccosh(np.sqrt(2)))


	def walkoff_length(self, first_beam, second_beam, **kwargs):
		beta11 = self.beta.transpose(first_beam.l0)[1]
		beta12 = self.beta.transpose(second_beam.l0)[1]
		DT = np.amax([first_beam.DT(FWHM=False), second_beam.DT(FWHM=False)])
		return DT/(np.sqrt(2)*np.abs(beta11-beta12))

	
	def soliton_number(self, the_beam, **kwargs):
		return np.sqrt(self.dispersion_length(the_beam,**kwargs)/self.Kerr_length(the_beam,**kwargs))


	def add_dopant(self, dopant_class, absorption={}, mode=(0,1), **kwargs):
		'''absorption must be a dictionnary with entries being the wavelength associated with the absorption
		in dB/m at this wavelength. For Double clad fibers, 'mode' should be set to mode_LP.Multimode()'''
	
		the_dopant = dopant_class(radius=self.core_radius, **kwargs)
	
		if len(absorption) > 0:
			l0 = np.array(list(absorption.keys()))
			absorption = np.array(list(absorption.values()))
	
			V = self.cutoff * 2.405 / l0
			confinement = np.zeros(len(l0))
			the_dopant_absorption = np.zeros(len(l0))

			the_mode = mode_LP.Multimode() if mode == 'multimode' else mode_LP.ModeLP(*mode)
	
			for i in range(len(l0)):
				self.set_mode_attributes(l0[i], the_mode)
				confinement[i] = the_mode.confinement
				the_dopant_absorption[i] = the_dopant.small_signal_absorption(l0[i], confinement[i])
	
			the_dopant.N = np.mean(absorption / the_dopant_absorption)

		self.dopants[the_dopant.symbol] = the_dopant
	
		return the_dopant



class DoubleCladFiber(Fiber):
	def __init__(self, clad_radius, **kwargs):
		super().__init__(**kwargs)
		
		self.clad_radius = clad_radius


	def set_mode_attributes(self, l0, mode):
		if isinstance(mode, mode_LP.ModeLP):
			mode.set_V(self.cutoff * 2.405 / l0)
			mode.set_radius(self.core_radius)
		elif isinstance(mode, mode_LP.Multimode):
			mode.set_radius(self.clad_radius)
			mode.set_confinement((self.core_radius/mode.radius)**2.)
		else:
			mode.set_radius(self.core_radius)


class LargeModeFiber(Fiber):
	def __init__(self, cutoff, area=None, radius=None):
		if area is not None:
			core_radius = np.sqrt(area/pi)
		elif radius is not None:
			core_radius = radius
		else:
			raise KeyError("Either 'area' or 'radius' must be specified")

		Fiber.__init__(self, cutoff, core_radius)

		# Glass
		self.beta = tools.Beta(1.04e-6, [0., 0., 18.24e-27, 0.042e-39])


	def set_mode_attributes(self, l0, mode):
		mode.radius = self.core_radius
		mode.confinement = 1


class LargeModeDoubleCladFiber(DoubleCladFiber):
	def __init__(self, cutoff, area=None, radius=None, clad_radius=62.5e-6):
		if area is not None:
			core_radius = np.sqrt(area/pi)
		elif radius is not None:
			core_radius = radius
		else:
			raise KeyError("Either 'area' or 'radius' must be specified")

		DoubleCladFiber.__init__(self, cutoff, core_radius, clad_radius)


	def set_mode_attributes(self, l0, mode):
		if isinstance(mode, mode_LP.ModeLP):
			mode.radius = self.core_radius
			mode.confinement = 1
		elif isinstance(mode, mode_LP.Multimode):
			mode.set_radius(self.clad_radius)
			mode.set_confinement((self.core_radius/mode.radius)**2.)
		else:
			mode.set_radius(self.core_radius)


# SMF-28 à 1550 nm
SMF28 = Fiber(1.26e-6, find_core_radius(1.26e-6,{1.55e-6:10.4e-6}))
#SMF28.beta = tools.Beta(1.55e-6, [0., 0., -23e-27, 0.12e-39])
SMF28.beta = tools.Beta(1.55e-6, [0., 0., -21.04e-27, 0.13e-39, -2.4e-54]) # These Louis Desbiens
SMF28.TR = 3e-15


# HI1060
HI1060 = Fiber(920e-9, find_core_radius(920e-9,{980e-9:5.9e-6, 1.06e-6:6.2e-6}))
HI1060.beta = tools.Beta(1.06e-6, [0., 0., -(1.06e-6**2./(2.*pi*c)) * (-38e-6), 74e-42]) #beta 3 : Kalaycioglu2010
HI1060.TR = 3e-15

# Thorlabs SM980 5.8 125
Thorlabs_SM980 = Fiber(920e-9, find_core_radius(920e-9,{980e-9:5.8e-6, 1064e-9:6.2e-6, 1550e-9:10.4e-6}))
Thorlabs_SM980.beta = tools.Beta(HI1060.beta.l0, HI1060.beta)

# OFS 980
OFS980_20 = Fiber(2*pi*1.8e-6*0.20/2.405, 1.8e-6)
OFS980_20.beta = tools.Beta(HI1060.beta.l0, HI1060.beta)


OFS980_16 = Fiber(2*pi*2.2e-6*0.16/2.405, 2.2e-6)
OFS980_16.beta = tools.Beta(OFS980_20.beta.l0, OFS980_20.beta)

# Large mode fiber
LargeModeFiber1600 = LargeModeFiber(920e-9, area=1600e-12)
LargeModeFiber3300 = LargeModeFiber(920e-9, area=3300e-12)
LargeModeFiber5000 = LargeModeFiber(920e-9, area=5000e-12)
LargeModeFiber50um = LargeModeFiber(920e-9, radius=25e-6)
LargeModeFiber80um = LargeModeFiber(920e-9, radius=40e-6)

LargeModeYbFiber3300 = LargeModeDoubleCladFiber(920e-9, area=3300e-12, clad_radius=130e-6)
LargeModeYbFiber3300.add_dopant(dopant.Ytterbium, {915e-9:5/0.8}, mode='multimode')

NKT_LMA_20 = Fiber(1.56e-6, 0.5 * 19.9e-6) # cutoff = 2 * pi * 0.5 * 19.9e-6 * 0.06 / 2.405
# Beta parameters from a fit on the dispersion curve between 950 nm and 1250 nm
NKT_LMA_20.beta = tools.Beta(1064e-9, [0., 0., 1.3964e-26, 4.9602e-41, -7.9778e-56]) 


# Liekki 30
Liekki30 = Fiber(900e-9, find_core_radius(900e-9,{1.55e-6:6.5e-6}))
Liekki30.beta = tools.Beta(1.55e-6, [0., 0., 12e-27])
Liekki30.TR = 3e-15
Liekki30.add_dopant(dopant.Erbium, {1.53e-6:30})

# Fibre SLA-IDF
OFS_Ultrawave_SLA = Fiber(1332e-9, find_core_radius(1332e-9,{1550e-9:11.9e-6}))
OFS_Ultrawave_SLA.beta = tools.Beta(1.55e-6, [0., 0., -19.5 * 1.55e-6**2 / (2 * pi * c)])

OFS_Ultrawave_IDF = Fiber(1332e-9, find_core_radius(1332e-9,{1555e-9:5.8e-6}))
OFS_Ultrawave_IDF.beta = tools.Beta(1.55e-6, [0., 0., 57e-27, -0.3e-39, -4e-55]) # Mesures Louis Desbiens


# Draka36
Draka36 = Fiber(1.3e-6, find_core_radius(1.3e-6,{1.55e-6:5.6e-6}))
Draka36.beta = tools.Beta(1.55e-6, [0., 0., 12e-27])
Draka36.TR = 3e-15
Draka36.add_dopant(dopant.Erbium, {1.532e-6:36})


# Generic Yb-doped fiber
GenericYbDopedFiber = Fiber(920e-9, find_core_radius(920e-9,{980e-9:5.9e-6, 1.06e-6:6.2e-6}))
GenericYbDopedFiber.beta = HI1060.beta
GenericYbDopedFiber.add_dopant(dopant.Ytterbium, N=2e25)


# Coractive Yb 115
CoractiveYb115 = Fiber(1000e-9, find_core_radius(1e-6,{1.06e-6:4e-6}))
CoractiveYb115.beta = HI1060.beta
CoractiveYb115.add_dopant(dopant.Ytterbium, {915e-9:50})


# Generic Nd doped fiber
GenericNdDopedFiber = Fiber(920e-9, find_core_radius(920e-9,{980e-9:5.9e-6, 1.06e-6:6.2e-6}))
GenericNdDopedFiber.add_dopant(dopant.Neodymium, N=2e25)


# Dual Yb-Nd doped fiber
YbNdDopedFiber = Fiber(920e-9, find_core_radius(920e-9,{980e-9:5.9e-6, 1.06e-6:6.2e-6}))
YbNdDopedFiber.add_dopant(dopant.Ytterbium, N=2e25)
YbNdDopedFiber.add_dopant(dopant.Neodymium, N=1e24)

# Coractive Yb 1604
CoractiveYb1604 = Fiber(836e-9, 2e-6)
CoractiveYb1604.beta = HI1060.beta
CoractiveYb1604.add_dopant(dopant.Ytterbium, {915e-9:44, 975e-9:220})


# Coractive Yb 1703
CoractiveYb1703 = Fiber(2*pi*1.75e-6*0.17/2.405, 1.75e-6)
CoractiveYb1703.beta = HI1060.beta
CoractiveYb1703.add_dopant(dopant.Ytterbium, {916e-9:502})


# Coractive Yb 1904
CoractiveYb1904 = Fiber(2*pi*1.9e-6*0.19/2.405, 1.9e-6)
CoractiveYb1904.beta = HI1060.beta
CoractiveYb1904.add_dopant(dopant.Ytterbium, {915e-9:490, 974e-9:2450})


# Coractive DCF_Yb_6_128
Coractive_DCF_Yb_6_128 = DoubleCladFiber(2*pi*3.25e-6*0.16/2.405, 3.25e-6, 64e-6)
Coractive_DCF_Yb_6_128.beta = LargeModeFiber1600.beta
Coractive_DCF_Yb_6_128.add_dopant(dopant.Ytterbium, {915e-9:0.65}, mode='multimode')


# Coractive DCF_Yb_10_128P
Coractive_DCF_Yb_10_128P = DoubleCladFiber(2*pi*5e-6*0.08/2.405, 5e-6, 64e-6)
Coractive_DCF_Yb_10_128P.beta = LargeModeFiber1600.beta
Coractive_DCF_Yb_10_128P.add_dopant(dopant.Ytterbium, {915e-9:1.7}, mode='multimode')


# Coractive HPA-Yb-10-01
Coractive_HPA_Yb1001 = DoubleCladFiber(2*pi*5.1e-6*0.19/2.405, 5.1e-6, 63e-6)
Coractive_HPA_Yb1001.beta = LargeModeFiber1600.beta
Coractive_HPA_Yb1001.add_dopant(dopant.Ytterbium, {915e-9:2.9}, mode='multimode')



